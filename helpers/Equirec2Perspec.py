import os, sys
import cv2
import numpy as np
import OpenEXR, Imath
from PIL import Image


def read_exr(exr_path):
    File = OpenEXR.InputFile(exr_path)
    PixType = Imath.PixelType(Imath.PixelType.FLOAT)
    DW = File.header()['dataWindow']
    Size = (DW.max.x - DW.min.x + 1, DW.max.y - DW.min.y + 1)
    rgb = [np.fromstring(File.channel(c, PixType), dtype=np.float32) for c in 'RGB']
    r = np.reshape(rgb[0], (Size[1], Size[0]))
    g = np.reshape(rgb[1], (Size[1], Size[0]))
    b = np.reshape(rgb[2], (Size[1], Size[0]))
    hdr = np.zeros((Size[1], Size[0], 3), dtype=np.float32)
    hdr[:, :, 0] = r
    hdr[:, :, 1] = g
    hdr[:, :, 2] = b
    return hdr

def write_exr(out_file, data):
    exr = OpenEXR.OutputFile(out_file, OpenEXR.Header(data.shape[1], data.shape[0]))
    red = data[:, :, 0]
    green = data[:, :, 1]
    blue = data[:, :, 2]
    exr.writePixels({'R': red.tostring(), 'G': green.tostring(), 'B': blue.tostring()})

class Equirectangular:
    def __init__(self, img_name):
        #check file extension for jpg or exr
        img_path, img_extension = os.path.splitext(img_name)
        
        if img_extension == ".exr":
            self._img = read_exr(img_name)
        elif img_extension == ".jpg" or img_extension == ".png":
            self._img = cv2.imread(img_name, cv2.IMREAD_COLOR)
        
        [self._height, self._width, _] = self._img.shape
        #cp = self._img.copy()  
        #w = self._width
        #self._img[:, :w/8, :] = cp[:, 7*w/8:, :]
        #self._img[:, w/8:, :] = cp[:, :7*w/8, :]
    

    def GetPerspective(self, FOV, THETA, PHI, height, width, RADIUS = 128):
        #
        # THETA is left/right angle, PHI is up/down angle, both in degree
        #

        #calculate pixels from centre of main image
        equ_h = self._height
        equ_w = self._width
        equ_cx = (equ_w - 1) / 2.0
        equ_cy = (equ_h - 1) / 2.0

        wFOV = FOV
        #height fov depends on shape of output image
        hFOV = float(height) / width * wFOV

        #calculate pixels from centre of cropped image
        c_x = (width - 1) / 2.0
        c_y = (height - 1) / 2.0

        #get fov angle from centre of image and convert to radians
        wangle = (180 - wFOV) / 2.0
        w_len = 2 * RADIUS * np.sin(np.radians(wFOV / 2.0)) / np.sin(np.radians(wangle))
        w_interval = w_len / (width - 1)

        hangle = (180 - hFOV) / 2.0
        h_len = 2 * RADIUS * np.sin(np.radians(hFOV / 2.0)) / np.sin(np.radians(hangle))
        h_interval = h_len / (height - 1)
        #setup spherical xyz coordinate map
        x_map = np.zeros([height, width], np.float32) + RADIUS
        y_map = np.tile((np.arange(0, width) - c_x) * w_interval, [height, 1])  #-width to width
        z_map = -np.tile((np.arange(0, height) - c_y) * h_interval, [width, 1]).T   #-height to height
        D = np.sqrt(x_map**2 + y_map**2 + z_map**2)
        xyz = np.zeros([height, width, 3], np.float)
        xyz[:, :, 0] = (RADIUS / D * x_map)[:, :]
        xyz[:, :, 1] = (RADIUS / D * y_map)[:, :]
        xyz[:, :, 2] = (RADIUS / D * z_map)[:, :]
        
        #get rotation matrices
        y_axis = np.array([0.0, 1.0, 0.0], np.float32)
        z_axis = np.array([0.0, 0.0, 1.0], np.float32)
        [R1, _] = cv2.Rodrigues(z_axis * np.radians(THETA))
        [R2, _] = cv2.Rodrigues(np.dot(R1, y_axis) * np.radians(-PHI))

        #rotate
        xyz = xyz.reshape([height * width, 3]).T
        xyz = np.dot(R1, xyz)
        xyz = np.dot(R2, xyz).T
        #convert spherical to lat lon coords
        lat = np.arcsin(xyz[:, 2] / RADIUS)
        lon = np.zeros([height * width], np.float)
        theta = np.arctan(xyz[:, 1] / xyz[:, 0])
        idx1 = xyz[:, 0] > 0
        idx2 = xyz[:, 1] > 0

        idx3 = ((1 - idx1) * idx2).astype(np.bool)
        idx4 = ((1 - idx1) * (1 - idx2)).astype(np.bool)
        
        lon[idx1] = theta[idx1]
        lon[idx3] = theta[idx3] + np.pi
        lon[idx4] = theta[idx4] - np.pi

        #shape lat lon to output size and convert to degrees
        lon = lon.reshape([height, width]) / np.pi * 180
        lat = -lat.reshape([height, width]) / np.pi * 180
        lon = lon / 180 * equ_cx + equ_cx
        lat = lat / 90 * equ_cy + equ_cy
    
        persp = cv2.remap(self._img, lon.astype(np.float32), lat.astype(np.float32), cv2.INTER_CUBIC, borderMode=cv2.BORDER_WRAP)
        return persp
        

def tonemapping(im, sv_path=None, gamma=2.4, percentile=50, max_mapping=0.5):
    # im = np.expm1(im)
    # hdr_path = sv_path.replace('.png', '.hdr')
    # hdr = im.astype('float32')
    # imageio.imwrite(hdr_path, hdr, format='hdr')

    power_im = np.power(im, 1.0 / gamma)
    # print (np.amax(power_im))
    non_zero = power_im > 0.0
    if non_zero.any():
        r_percentile = np.percentile(power_im[non_zero], percentile)
    else:
        r_percentile = np.percentile(power_im, percentile)
    alpha = max_mapping / (r_percentile + 1e-10)
    tonemapped_im = np.multiply(alpha, power_im)

    tonemapped_im = np.clip(tonemapped_im, 0, 1)

    if sv_path:
        hdr = tonemapped_im * 255.0
        hdr = Image.fromarray(hdr.astype('uint8'))
        hdr.save(sv_path)

    return tonemapped_im.astype('float32')

if __name__ == "__main__":
    img_filename = sys.argv[1]

    crop = Equirectangular(img_filename)

    hdr_crop = crop.GetPerspective(90, 180, -135, 224, 224)

    cv2.imshow("-135 perspective",hdr_crop)
    cv2.waitKey(0)
