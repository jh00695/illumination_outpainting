import os, sys
import csv
import cv2
import imageio as im
import random
import math
import numpy as np
import pandas as pd
import argparse
from PIL import Image
from skimage.feature import canny
from skimage.color import rgb2gray
import matplotlib.pyplot as plt

import torch
#sys.path.insert(0, 'C:/Users/Jack/Documents/University/PhD/Code/Object_relighting/Illumination_Outpainting')
sys.path.insert(0, '/mnt/fast/nobackup/users/jh00695/Illumination_Outpainting')
from helpers.equiRotate import rotate_equirect
from networks.loss import cosine_blur

"""
	To Do:
	- add bit for generating masks
"""

def parse_arguments(args):

	parser = argparse.ArgumentParser()
	parser.add_argument('--dataset_dir', type=str, default="../dataset/EXR_images")
	parser.add_argument('--augments', type=int, default=3)
	parser.add_argument('--out_dir', type=str, default="../dataset/images")
	parser.add_argument('--csv_file', type=str, default="../dataset/dataset.csv")
	parser.add_argument('--env_dir', type=str, default="../dataset/env_maps")
	parser.add_argument('--edge_dir', type=str, default="../dataset/edge_maps")
	parser.add_argument('--ldr_dir', type=str, default="../dataset/ldr_images")
	parser.add_argument('--gamma_dir', type=str, default="../dataset/images_gamma")
	parser.add_argument('--train_split', type=int, default=80)
	parser.add_argument('--val_split', type=int, default=10)


	return parser.parse_known_args()


def process_dataset(dataset_dir, out_dir, csv_file, num_augs):
	#get list of images in dataset_dir
	img_list = get_file_list(dataset_dir)
	#create csv file
	with open(csv_file, 'w') as csvDataFile:
		writer = csv.writer(csvDataFile)
		header = ["filename"]
		writer.writerow(header)
		#cycle thorugh img_list
		for img_file in img_list:
			#read image
			hdr = im.imread(img_file, 'EXR-FI')
			#resize
			hdr = cv2.resize(hdr, (512,256), interpolation=cv2.INTER_CUBIC)
			#save to CSV file
			img_filename, img_extension = os.path.splitext(os.path.basename(img_file))
			writer.writerow([str(img_filename)])
			#save image
			im.imwrite(out_dir+"/"+img_filename+img_extension, hdr)
			#augment by rotating
			completed = []
			for i in range(num_augs):
				#add random augmentations
				check = False
				while check == False:
					# add random vertical flips
					r = random.randrange(0,5,1) #20% chance of vertically flipping
					#add random rotations
					rot = random.randrange(60,300,60)	#random roation in degrees between 30 and 330
					if rot not in completed:
						completed.append(rot)
						check = True

				img_aug = rotate_equirect(hdr,(rot,0,0))
				if r == 1:
					img_aug = cv2.flip(img_aug, 1)
					print("{}: rotation {} flip: Vertical".format(i,rot))
					img_out = "{}_{}_V".format(img_filename,rot)
				else:
					print("{}: rotation {}".format(i,rot))
					img_out = "{}_{}".format(img_filename,rot)

				writer.writerow([str(img_out)])

				#save augmented file
				im.imwrite(out_dir+"/"+img_out+img_extension, img_aug)

		#save df to csv file
		csvDataFile.close()


def get_file_list(img_dir):
	listOfFile = os.listdir(img_dir)
	allFiles = list()

	for entry in listOfFile:
		fullPath = os.path.join(img_dir, entry)

		if os.path.isdir(fullPath):
			allFiles = allFiles + get_file_list(fullPath)
		else:
			allFiles.append(fullPath)

	return allFiles

def randomise_split(dataset_dir, csv_file, split, augs):
	#randomise train and test from outdoor and indoor datasets into dataset/train and dataset/test

	#read csv file of dir
	img_df = pd.read_csv(csv_file)

	#create new df for test and train
	train_df = pd.DataFrame(columns=['filename'])
	val_df = pd.DataFrame(columns=['filename'])
	test_df = pd.DataFrame(columns=['filename'])

	train_df, val_df, test_df = r_split(dataset_dir, img_df, train_df, val_df, test_df, split, augs)

	#create new csv from index of test
	train_df.to_csv(dataset_dir + "/train.csv",index=False)
	val_df.to_csv(dataset_dir + "/val.csv",index=False)
	test_df.to_csv(dataset_dir + "/test.csv",index=False)


def r_split(path, img_df, train_df, val_df, test_df, split, augs):
	#randomly index a set percent for test and move to test dir

	img_list = img_df['filename'].tolist()
	train_split = math.floor((img_df.shape[0]/(augs+1))*(split[0]/100))
	val_split = math.floor((img_df.shape[0]/(augs+1))*(split[1]/100))

	print("\ntraining split: ", split[0])
	for i in range(train_split):
		rand  = random.randrange(0, len(img_list), 1)
		img_name = img_list[rand]

		row_df = pd.DataFrame([img_name], columns=['filename'])

		#if not check:
		if train_df.empty:
			train_df = row_df
		else:
			train_df = train_df.append(row_df, ignore_index=True)
		
		img_list.pop(rand)
		#repeat below for all augmetnations of original image
		#get list of every image of that name
		repeat = augs
		rmv_list = []
		for img in img_list:
			if img_name[:10] in img:
				row_df = pd.DataFrame([img], columns=['filename'])
				train_df = train_df.append(row_df, ignore_index=True)

				rmv_list.append(img)

				repeat -= 1

			if not repeat: break

		for img in rmv_list: img_list.remove(img)


	#print(train_df)

	print("validation split: ",split[1])
	for i in range(val_split):
		rand  = random.randrange(0, len(img_list), 1)
		img_name = img_list[rand]

		row_df = pd.DataFrame([img_name], columns=['filename'])

		if val_df.empty:
			val_df = row_df
		else:
			val_df = val_df.append(row_df, ignore_index=True)
		
		img_list.pop(rand)
		#repeat below for all augmetnations of original image
		#get list of every image of that name
		repeat = augs
		rmv_list = []
		for img in img_list:
			if img_name[:10] in img:
				row_df = pd.DataFrame([img], columns=['filename'])
				val_df = val_df.append(row_df, ignore_index=True)

				rmv_list.append(img)

				repeat -= 1

			if not repeat: break

		for img in rmv_list: img_list.remove(img)

	#print(val_df)

	print("testing split: ", round(1-(split[0]+split[1])/100,3))
	for img_name in img_list:
		row_df = pd.DataFrame([img_name], columns=['filename'])

		if test_df.empty:
			test_df = row_df
		else:
			test_df = test_df.append(row_df, ignore_index=True)

	#print(test_df)

	return(train_df, val_df, test_df)

def tonemapping(im, sv_path=None, gamma=2.4, percentile=50, max_mapping=0.5):
    # im = np.expm1(im)
    # hdr_path = sv_path.replace('.png', '.hdr')
    # hdr = im.astype('float32')
    # imageio.imwrite(hdr_path, hdr, format='hdr')

    power_im = np.power(im, 1.0 / gamma)
    # print (np.amax(power_im))
    non_zero = power_im > 0.0
    if non_zero.any():
        r_percentile = np.percentile(power_im[non_zero], percentile)
    else:
        r_percentile = np.percentile(power_im, percentile)
    alpha = max_mapping / (r_percentile + 1e-10)
    tonemapped_im = np.multiply(alpha, power_im)

    tonemapped_im = np.clip(tonemapped_im, 0, 1)

    if sv_path:
        hdr = tonemapped_im * 255.0
        hdr = Image.fromarray(hdr.astype('uint8'))
        hdr.save(sv_path)

    return tonemapped_im.astype('float32')

def gamma_correct(img, gamma=2.4, alpha=0.5, inverse=0):
    clamped = np.where(img<0,0,img)
#    alpha = alpha/np.percentile(img,50)
    if inverse:
        return np.power(np.divide(clamped,alpha),gamma)
    else:
        return alpha*np.power(clamped,1/gamma)

def gamma_correct_torch(img, gamma=2.2, alpha=30, inverse=0, device=torch.device("cpu")):
    clamped = torch.where(img<0,torch.zeros(1).float().to(device),img)
#    clamped = torch.where(img<0,int(0),img)
    if inverse:
        return torch.pow(torch.div(clamped,alpha),gamma)
    else:
        return alpha*torch.pow(clamped,1/gamma)


if __name__ == "__main__":
	device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
	print('Using device: ', device, ":", torch.cuda.current_device())

	args, unknown = parse_arguments(sys.argv)

	#augment dataset and get list of data in csv
#	print("Augmenting dataset in ", args.dataset_dir)
#	process_dataset(args.dataset_dir, args.out_dir, args.csv_file, args.augments)

	#generate env_maps by processing in batches of 64
#	print("Genereating Environment Maps...")
#	img_list = get_file_list(args.out_dir)
#	batch_size = 64
#
#	blur_func = cosine_blur((batch_size,3,256,512), device)
#
#	img_batch = torch.zeros((batch_size,3,256,512),dtype=torch.float,device=device)
#	batch_list = []
#	num_batches = 0
#	for i,img_file in enumerate(img_list):
#		batch_list.append(img_file)
#		cur_index = i-(num_batches*batch_size)
#		hdr = im.imread(img_file, 'EXR-FI')
#		hdr = im.imread(img_file)
#		#hdr to tensor
#		img_batch[cur_index] = torch.Tensor(hdr).to(device).permute((2,0,1))
#
#		#process batch when it is full
#		if cur_index == batch_size-1 or i+1 == len(img_list):
#			num_batches +=1
#			#get env_maps
#			env_maps = blur_func(img_batch, 1, 1).permute((0,2,3,1))
#			#save to dir
#			for b, img_name in enumerate(batch_list):
#				img_filename, img_extension = os.path.splitext(os.path.basename(img_name))
#				im.imwrite(args.env_dir + img_filename + img_extension, env_maps[b].squeeze(0).cpu().detach().numpy())
#			batch_list = []

	#tone mapping and edge detection
#	img_list = get_file_list(args.out_dir)
#	for img in img_list:
#		toned_img = tonemapping(im.imread(img, 'EXR-FI'), gamma=2.2)
#		scaled = cv2.resize(toned_img, (256,128), interpolation=cv2.INTER_CUBIC)
#		gray_img = rgb2gray(scaled)
#		boarder_img = cv2.copyMakeBorder(gray_img, 50, 50, 50, 50, cv2.BORDER_REFLECT)
#		edge_img = canny(boarder_img, sigma=2).astype(np.float)
#		edge_img = edge_img[50:-50,50:-50]
#		img_filename, img_extension = os.path.splitext(os.path.basename(img))
#		im.imwrite(args.edge_dir+"/"+img_filename+".png", edge_img)
#		im.imwrite(args.ldr_dir+"/"+img_filename+".png", toned_img)
	df = pd.read_csv(args.csv_file)
	img_list = df['filename']
#	img_list = get_file_list(args.dataset_dir)
	print("len image list: ",len(img_list))
	ma = 0
	mi = 1
	h_acc = 0
	p = [0.0,0.0,0.0]
	hdr_tot = []
	for img in img_list:
		hdr = im.imread(args.dataset_dir+img+".exr", 'EXR-FI')
#		hdr = gamma_correct(hdr,gamma=2.4,alpha=0.5/np.percentile(hdr,50))
#		img_filename = os.path.basename(img)
#		im.imwrite(args.gamma_dir + img_filename, hdr)
#		ones = np.ones_like(hdr)
#		hdr = hdr - ones
#		hdr = np.multiply(hdr,2) - ones
#		img_1 = hdr +ones
#		hdr_pos = np.where(img_1 < 1, 1,img_1)
#		hdr = np.clip(np.log(hdr_pos)-ones,-1,1000)

#		print("\nhdr max: {}, min: {}".format(hdr.max(),hdr.min()))
		if hdr.max() > ma:
			ma = hdr.max()
		if hdr.min() < mi:
			mi = hdr.min()
#		print("P 10: {}, 95: {}, 99: {}".format(np.percentile(hdr,10),np.percentile(hdr,75),np.percentile(hdr,90)))

		p[0] += np.percentile(hdr,25)
		p[1] += np.percentile(hdr,50)
		p[2] += np.percentile(hdr,75)
		h_acc += cv2.calcHist(rgb2gray(hdr), [0], None, [500],[0,100])

		s_hdr = cv2.resize(hdr, (128,64),interpolation=cv2.INTER_CUBIC)
		hdr_tot = np.append(hdr_tot,s_hdr.flatten(),axis=0)


#	h_acc /= len(img_list)
	for i in range(3):
		p[i] = p[i]/len(img_list)
	print("P 25: {}, 50: {}, 75: {}".format(p[0],p[1],p[2]))
	print("Max: {}, Min: {}".format(ma,mi))
#	plt.plot(h_acc)
#	plt.savefig(args.out_dir+"accumulated_histHDR.png")


	x = np.linspace(0,1,100)
	x = np.append(x,np.linspace(1,100,99),axis=0)

	y = x**(1/2.4)
	plt.plot(x,y,label="Gamma 2.4")

	y = x**(1/2.8)
	plt.plot(x,y,label="Gamma 2.8")

	y = x**(1/3)
	plt.plot(x,y,label="Gamma 3")

	y = x**(1/4)
	plt.plot(x,y,label="Gamma 4")

	y = np.log(x+1)
	plt.plot(x,y,label="Natural Log")

	plt.boxplot(hdr_tot, vert=False)

	plt.legend()
	plt.title("Normalisation Methods")
	plt.xlim(right=1,left=-0.5)
	plt.ylim(top=2)
	plt.savefig(args.out_dir+"test_stats.png")

	#randomise split
	#print("Splitting into Subsets... train: {}, val: {}, test: {}".format(args.train_split,args.val_split,int(100-args.train_split-args.val_split)))
	#randomise_split(args.out_dir, args.csv_file, [args.train_split, args.val_split], args.augments)
