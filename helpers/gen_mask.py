import os, sys
import imageio as im
import numpy as np
import cv2

"""
	To Do:
	- 118,147(2),158,163(4),167,169(6),171,172(8),176(16),178(32),179(64)
	- put in data processing file?
"""

def gen_mask(height,width,angle):
	mask=np.full((height,width),255,dtype=np.uint8)
	
	if angle >= 179:
		c_angle = (angle/360)/2
		mask[...,:int(c_angle*width)] = 0
		mask[...,int((1-c_angle)*width):] = 0
	else:
		mod = 1
		if angle > 117:
			mod = 2
		if angle > 146:
			mod = 3
		if angle > 157:
			mod = 4
		if angle > 162:
			mod = 5
		if angle > 166:
			mod = 6
		if angle > 170:
			mod = 8
		if angle > 171:
			mod = 16
		if angle > 175:
			mod = 32
		if angle > 177:
			mod = 64
		mask = GetMask(mask,angle,180,0,height*mod,width*mod)
	im.imwrite("../dataset/masks/mask_{}.png".format(str(angle)),mask)

def GetMask(img, FOV, THETA, PHI, height, width, RADIUS = 1024):
	#
	# THETA is left/right angle, PHI is up/down angle, both in degree
	#
	h, w = img.shape[0:2]
	#calculate pixels from centre of main image
	equ_h = h
	equ_w = w
	equ_cx = (equ_w - 1) / 2.0
	equ_cy = (equ_h - 1) / 2.0

	wFOV = FOV
	#height fov depends on shape of output image
	#hFOV = float(height) / width * wFOV
	hFOV = FOV

	#calculate pixels from centre of cropped image
	c_x = (width - 1) / 2.0
	c_y = (height - 1) / 2.0

	#get fov angle from centre of image and convert to radians
	wangle = (180 - wFOV) / 2.0
	#wangle = (-1* wFOV) / 2.0
	w_len = 2 * RADIUS * np.sin(np.radians(wFOV / 2.0)) / np.sin(np.radians(wangle))
	w_interval = w_len / (width - 1)

	hangle = (180 - hFOV) / 2.0
	#hangle = ( -1* hFOV) / 2.0
	h_len = 2 * RADIUS * np.sin(np.radians(hFOV / 2.0)) / np.sin(np.radians(hangle))
	h_interval = h_len / (height - 1)
	#setup spherical xyz coordinate map
	x_map = np.zeros([height, width], np.float32) + RADIUS
	y_map = np.tile((np.arange(0, width) - c_x) * w_interval, [height, 1])  #-width to width
	z_map = -np.tile((np.arange(0, height) - c_y) * h_interval, [width, 1]).T   #-height to height
	D = np.sqrt(x_map**2 + y_map**2 + z_map**2)
	xyz = np.zeros([height, width, 3], np.float64)
	xyz[:, :, 0] = (RADIUS / D * x_map)[:, :]
	xyz[:, :, 1] = (RADIUS / D * y_map)[:, :]
	xyz[:, :, 2] = (RADIUS / D * z_map)[:, :]

	#get rotation matrices
	y_axis = np.array([0.0, 1.0, 0.0], np.float32)
	z_axis = np.array([0.0, 0.0, 1.0], np.float32)
	[R1, _] = cv2.Rodrigues(z_axis * np.radians(THETA))
	[R2, _] = cv2.Rodrigues(np.dot(R1, y_axis) * np.radians(-PHI))

	xyz = xyz.reshape([height * width, 3]).T
	xyz = np.dot(R1, xyz)
	xyz = np.dot(R2, xyz).T
	lat = np.arcsin(xyz[:, 2] / RADIUS)
	lon = np.zeros([height * width], np.float64)
	theta = np.arctan(xyz[:, 1] / xyz[:, 0])
	idx1 = xyz[:, 0] > 0
	idx2 = xyz[:, 1] > 0

	idx3 = ((1 - idx1) * idx2).astype(np.bool_)
	idx4 = ((1 - idx1) * (1 - idx2)).astype(np.bool_)

	lon[idx1] = theta[idx1]
	lon[idx3] = theta[idx3] + np.pi
	lon[idx4] = theta[idx4] - np.pi

	#shape lat lon to output size and convert to degrees
	lon = lon.reshape([height, width]) / np.pi * 180
	lat = -lat.reshape([height, width]) / np.pi * 180
	lon = lon / 180 * equ_cx + equ_cx
	lat = lat / 90 * equ_cy + equ_cy

	lon = np.around(lon.flatten()).astype(int)
	lat = np.around(lat.flatten()).astype(int)
	ll = lat*w +lon
	img = img.flatten()

	np.put(img,ll,0)
	img = np.reshape(img,[h,w])

	return img

if __name__ == '__main__':
	init = int((1-3.125/100)*360)
	masks = np.arange(60,init,(init-60)/32)
	for i in masks:
		gen_mask(256,512,i)