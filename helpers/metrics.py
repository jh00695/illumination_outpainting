import numpy as np

import torch
import torch.nn as nn
import torchvision

from .ParametricLights import extract_lights_from_equirectangular_image, calculate_angular_error_metric
from .FIDCalculations import calculate_fid, inception_feature_for_equirectangular

"""
    To Do:
    - add metrics
        -pixelwise RMSE
    - convert angular_error to pytorch for swifter running times
"""
class EdgeAccuracy(nn.Module):
    """
    Measures the accuracy of the edge map
    """
    def __init__(self, threshold=0.5):
        super(EdgeAccuracy, self).__init__()
        self.threshold = threshold

    def __call__(self, inputs, outputs):
        labels = (inputs > self.threshold)
        outputs = (outputs > self.threshold)

        relevant = torch.sum(labels.float())
        selected = torch.sum(outputs.float())

        if relevant == 0 and selected == 0:
            return torch.tensor(1), torch.tensor(1)

        true_positive = ((outputs == labels) * labels).float()
        recall = torch.sum(true_positive) / (relevant + 1e-8)
        precision = torch.sum(true_positive) / (selected + 1e-8)

        return precision.item(), recall.item()


class PSNR(nn.Module):
    def __init__(self, max_val):
        super(PSNR, self).__init__()

        base10 = torch.log(torch.tensor(10.0))
        max_val = torch.tensor(max_val).float()

        self.register_buffer('base10', base10)
        self.register_buffer('max_val', 20 * torch.log(max_val) / base10)

    def __call__(self, a, b):
        mse = torch.mean((a.float() - b.float()) ** 2)

        if mse == 0:
            return torch.tensor(0)

        return self.max_val - 10 * torch.log(mse) / self.base10

class angular_error(nn.Module):
    def __init__(self):
        super(angular_error, self).__init__()

    def __call__(self, inputs, outputs):
        err = 0.0
        for b in range(inputs.shape[0]):
            l1 = extract_lights_from_equirectangular_image(outputs[b].permute((1,2,0)).detach().cpu().numpy())
            l2 = extract_lights_from_equirectangular_image(inputs[b].permute((1,2,0)).detach().cpu().numpy())

            err += calculate_angular_error_metric(l1, l2)

        return err/inputs.shape[0]

class my_FID(nn.Module):
    def __init__(self,device):
        super(my_FID, self).__init__()
        self.incp_img_size = 299
        self.incp_model = torchvision.models.inception_v3(pretrained=True,progress = False).to(device)
        self.incp_model.eval()
        self.device = device

    def __call__(self, inputs, outputs):
        ft1 = inception_feature_for_equirectangular(inputs, self.incp_img_size, self.incp_model,self.device)
        ft2 = inception_feature_for_equirectangular(outputs, self.incp_img_size, self.incp_model,self.device)

        fid = calculate_fid(ft1, ft2)

        return fid

#relative radiance accuracy (as used by DeepLight)
class RRA(nn.Module):
    def __init__(self):
        super(RRA, self).__init__()

    def __call__(self, inputs, outputs):
        red_rra = torch.mean((outputs[...,0,:]-inputs[...,0,:])/inputs[...,0,:])*100
        green_rra = torch.mean((outputs[...,1,:]-inputs[...,1,:])/inputs[...,1,:])*100
        blue_rra = torch.mean((outputs[...,2,:]-inputs[...,2,:])/inputs[...,2,:])*100
        return red_rra.item(), green_rra.item(), blue_rra.item()
