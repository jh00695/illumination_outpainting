import os, sys
import imageio as im
import numpy as np
import argparse

from dataset_processing import get_file_list

def parse_arguments(args):

	parser = argparse.ArgumentParser()
	parser.add_argument('--dataset_dir', type=str, default="../dataset/EXR_images")

	return parser.parse_known_args()

if __name__ == "__main__":
	args, unknown = parse_arguments(sys.argv)

	img_list = get_file_list(args.dataset_dir)

	tot = np.empty([2,len(img_list)])
	MAX = 0
	MIN = 1
	cap_max = 0
	cap_min = 0
	for i,img_name in enumerate(img_list):
		hdr = im.imread(img_name, 'EXR-FI')
		tot[0][i] = hdr.max()
		tot[1][i] = hdr.min()

		if tot[0][i] > MAX:
			MAX = tot[0][i]
		if tot[1][i] < MIN:
			MIN = tot[1][i]

		if tot[0][i] > 1:
			cap_max += 1
		if tot[1][i] < 0:
			cap_min += 1

	print("Stats:")
	print("Max: ", MAX)
	print("Cap: ", cap_max)
	print("Min: ", MIN)
	print("Cap: ", cap_min)
	print("Mean Max: ", np.mean(tot[0]))
	print("Mean Min: ", np.mean(tot[1]))
