import os, sys
import math
import argparse
import numpy as np
import pandas as pd
import random
import matplotlib.pyplot as plt
from pathlib import Path
from contextlib import suppress

import networks.networks as network
from networks.ill_outpainting import *
import helpers.dataset_processing as data

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import torchvision.transforms as transforms
import torchvision.models as models
import torch.optim as optim
from torch.utils.data import DataLoader
from torchsummary import summary

"""
	To Do:
	- test bigger resolution
	- decrease batch size
	
	- (3) check outputs of cosine loss function

	!!!- test splitting hdr and ldr into seperate channels
	!!!- metrics for outdoor vs indoor methods
	!!!- get histogram of lighting errors

	- load best validation version for test results

	- try different networks like pix2pix
	- test edgeconnect with short cuts
	- (2/3) compare env net (sommanth & kurz) and others to see if a more complicated entwork amy be better

	- (1) use POAI datasets
	- (1) change masks to different FOV, also add masks to random spot of image

"""

def parse_arguments(args):
	usage_text = (
		"Training script for Illumination Outpainting Model"
		"Usage: python train.py"
	)
	parser = argparse.ArgumentParser(description=usage_text)
	#directories
	parser.add_argument('--path', type=str, default='./', help='current directory path')
	parser.add_argument('--dataset_dir', type=str, default='/dataset', help='Location of dataset')
	parser.add_argument('--save_dir', type=str, default='/models', help='Directory to save model')
	parser.add_argument('--output_dir', type=str, default='/outputs', help='Directory to save output files')
	parser.add_argument('--name', type=str, default='test', help='Name of test')
	parser.add_argument('--load_dir', type=str, default='/models/saves', help='Directory to model to Load')
	parser.add_argument('--load', type=int, default=0, help='Load module, binary input')
	parser.add_argument('--checkpoint', type=int, default=0, help='Load module from last checkpoint')
	parser.add_argument('--check', type=int, default=1, help='Checkpoint succesful')
	#mode
	parser.add_argument('--mode', type=int, default=2, help='1: Train, 2: Train & Test, 3: Test, 4: Load test, Other: Evaluate')
	#networks
	parser.add_argument('--model', type=int, default=1, help='1:Edge, 2:Env_map, 3:Outpaint, 4:Env + Out, 5:Edge + Out, 6:All ')
	parser.add_argument('--width', type=int, default=512, help='Width of input image')
	parser.add_argument('--height', type=int, default=256, help='Height of input image')
	parser.add_argument('--batch_size', type=int, default=16, help='Batch Size')
	parser.add_argument('--colour', type=int, default=3, help='Colour of input image')
	parser.add_argument('--epochs', type=int, default=100, help='Number of Epochs')
	parser.add_argument('--res', type=int, default=256, help='Resolution of Cosine blur image')
	parser.add_argument('--noise', type=int, default=2, help='none, gaussian or uniform noise')
	parser.add_argument('--network', type=int, default=0, help='Network type, 0:UNet, 1: Edgeconnect')
	parser.add_argument('--LDR', type=int, default=0, help='LDR input and output')
	parser.add_argument('--L2HDR', type=int, default=0, help='LDR input and HDR output')
	#general
	parser.add_argument('--seed', type=int, default=10, help='Seed for randomisation')
	parser.add_argument('--log_norm', type=int, default=7, help='Log normalisation (clamped at 0)')
	parser.add_argument('--gamma', type=float, default=2.8, help='Gamma value for log normalisation')
	parser.add_argument('--act_layer', type=str, default=None, help='Activation layer elu or relu, default leakyReLU')
	parser.add_argument('--dsc_act_layer', type=str, default=None, help='Activation layer elu or relu, default leakyReLU')
	parser.add_argument('--sigmoid', type=int, default=0,help='Use sigmoid activation for UNet')
	parser.add_argument('--log_alpha', type=float, default=1,help="Alpha scaling value for log normalisation")
	parser.add_argument('--upsample', type=str, default='bilinear',help="Upsample interpolation")
	parser.add_argument('--init_weights', type=str, default='True',help="Discriminator weight initialisation")
	parser.add_argument('--singleOUT', type=int, default=0,help='Use all three data types for image completionb network')
	#mask gneration
	parser.add_argument('--ICN_ldr', type=int,default=0,help="idk ahhh")
	parser.add_argument('--mask', type=int, default=1, help='0: generated, 1: loaded')
	parser.add_argument('--mask_steps', type=int, default=5, help='Number of mask steps')
	parser.add_argument('--mask_dif', type=float, default=355, help='percentage difference at starting mask')
	parser.add_argument('--mask_epoch', type=int, default=1000, help='Reduce mask angle per this amopunt of epochs')
	parser.add_argument('--mask_lim', type=float, default=0.19, help='Reduce mask angle when generator loss drops below this value')
	parser.add_argument('--mask_start', type=int, default=110, help='Mask value to start on, over 100 means defaultmask start value of 350')
	parser.add_argument('--overfit', type=float, default = 10, help='If this value is reach by the loss function then the model has overfit and will be reset to last save')
	#edge generation
	parser.add_argument('--edge', type=int, default=0, help='1: generated, 0: loaded')
	parser.add_argument('--nms', type=int, default=0)
	parser.add_argument('--sigma', type=int, default=0, help='Standard Deviation of the Gaussian')
	#loss functions
	parser.add_argument('--EDGE_THRESHOLD', type=float, default='0.5')

	parser.add_argument('--fm_lw', type=float, default='10')
	parser.add_argument('--edg_l1_lw', type=float, default='10')

	parser.add_argument('--env_l1_lw', type=float, default=10) #more wieght should be put on l2 for the env map as it is important to get it as accurate as possible to the GT rather than just looking believable
	parser.add_argument('--env_l1_hdr_lw', type=float, default=10) #more wieght should be put on l2 for the env map as it is important to get it as accurate as possible to the GT rather than just looking believable

	parser.add_argument('--l1_lw', type=float, default='1')
	parser.add_argument('--style_lw', type=float, default='250')
	parser.add_argument('--cos_lw', type=float, default='1000')
	parser.add_argument('--alpha', type=int, default=1)
	parser.add_argument('--content_lw', type=float, default='0.1')
	parser.add_argument('--outpaint_adv_lw', type=float, default='0.1')
	
	#gan settings
	parser.add_argument('--out_loss', type=str, default='nsgan')
	parser.add_argument('--edge_loss', type=str, default='nsgan')
	parser.add_argument('--env_loss', type=str, default='nsgan')
	#ADAM Optimiser
	parser.add_argument('--lr', type=float, default=0.0001, help='Learning rate')
	parser.add_argument('--d2g_lr', type=float, default=0.1, help='Discriminator/Generator Learning rate')
	parser.add_argument('--beta1', type=float, default=0.0, help='Beta 1')
	parser.add_argument('--beta2', type=float, default=0.9, help='Beta 2')

	return parser.parse_known_args()


def run(args, device):
	path = args.path

	# initialize random seed
	#torch.manual_seed(args.seed)
	#torch.cuda.manual_seed_all(args.seed)
	np.random.seed(args.seed)
	random.seed(args.seed)

	# build the model and initialize
	model = IllOutPainting(args, device)

	if args.model == 6 or args.load:
		model.load()
		model.start_epoch = 0

	if args.checkpoint == 1:
		model.checkpoint()
		if args.model == 1:
			epoch = model.edge_model.start_epoch
		elif args.model == 2:
			epoch = model.env_model.start_epoch
		elif args.model >= 3:
			epoch = model.outpaint_model.start_epoch

	if args.mask_start < 100:
		model.start_mask = args.mask_start

	# model training
	if args.mode <= 2:
		print('\nstart training...\n')
		model.train()

	# model test
	elif args.mode == 3:
		print('\nstart testing...\n')
		model.test(epoch)
	elif args.mode == 4:
		#print('\nPrinting Testset...\n')
		model.load()
		model.print_testset(epoch)
	# eval mode
	else:
		print('\nstart eval...\n')
		model.eval(epoch)


#main
if __name__ == '__main__':
	device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

	print('Using device: ', device, ":", torch.cuda.current_device())
	args, unknown = parse_arguments(sys.argv)
	print("\nArguments:\n")
	for arg in vars(args):
		print(" {} : {} ".format(arg, getattr(args,arg) or ''))
	run(args,device)

	if device.type == "cuda":
		print("Memory Usage:")
		print("Allocated: ", round(torch.cuda.memory_allocated(device)/(1024**2),1), 'MB')
		print("Cached: ", round(torch.cuda.memory_reserved(device)/(1024**2),1), 'MB')
