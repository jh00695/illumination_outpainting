import os
import glob
import scipy
import torch
import random
import numpy as np
import pandas as pd
import cv2
import torchvision.transforms.functional as F
from torch.utils.data import DataLoader
from PIL import Image
import imageio as im
from skimage.feature import canny
from skimage.color import rgb2gray, gray2rgb
from helpers.gen_mask import gen_mask
from helpers.dataset_processing import gamma_correct_torch

import time

"""
    Imports the data into the dataset:
    - GT env maps
    - GT edge maps
    - GT HDRIs
    Generate:
    - Crops (in equirectangular format) is just mask times HDRI
    - Mask from 0 values of crop in equirectangular format
      could load as there are only a certain amount of masks
      and all are the same for each bit of data
    
    To Do:
    - check create_iterator function
"""

class Dataset(torch.utils.data.Dataset):
    def __init__(self, args, csv_file, training=True):
        super(Dataset, self).__init__()
        self.training = training

        self.width = args.width
        self.sigma = args.sigma
        self.edge = args.edge
        self.mask = args.mask
        self.nms = args.nms
        self.log = args.log_norm
        self.gamma = args.gamma
        self.alpha = args.log_alpha
        self.model = args.model
        self.LDR = args.LDR
#        init = int((1-args.mask_dif/100)*360)
#        self.mask_angle = np.arange(60,init,(init-60)/args.mask_steps,dtype=int)
        init = args.mask_dif
        self.mask_angle = np.arange(90,init,args.mask_steps,dtype=int)
#        print("\nDEBUG!! mask angles: ", self.mask_angle)
        self.mask_step = len(self.mask_angle)-1

        assert os.path.exists(args.path + args.dataset_dir)

        df = pd.read_csv(csv_file)
        img_list = df['filename']

        self.img_list = img_list
        self.root_dir = args.path + args.dataset_dir

        #self.to_tensor = transforms.ToTensor()

    def __len__(self):
        return len(self.img_list)

    def __getitem__(self, index):
        try:
            item = self.load_item(index)
        except:
            print('loading error: ' + self.img_list[index])
            item = self.load_item(0)

        return item

    def load_name(self, index):
        name = self.img_list[index]
        return os.path.basename(name)

    def load_item(self, index):
        hdr_means = np.zeros(2)
        # load EXR (used as final check)
        img_name = os.path.join(self.root_dir, "images", self.img_list[index] + ".exr")
        img = im.imread(img_name, 'EXR-FI')
        img = cv2.resize(img, (self.width,int(self.width/2)), interpolation=cv2.INTER_CUBIC)
        in_out_img = np.zeros_like(img)
        in_out_img[...,int(self.width/2):,:] = img[...,:int(self.width/2),:]
        in_out_img[...,:int(self.width/2),:] = img[...,int(self.width/2):,:]
        if self.log > 2:
            hdr_means[0] = np.percentile(in_out_img,50)
        else:
            hdr_means[0] = np.mean(in_out_img)
        io_img_t = self.to_tensor(in_out_img)
        if self.log:
            io_img_t = self.log_norm_t(io_img_t, self.log)

        # create grayscale image
        img_gray = rgb2gray(in_out_img)

        #load env_map
        if self.LDR == 0:
            env_name = os.path.join(self.root_dir, "env_maps", self.img_list[index] + ".exr")
            env_map = im.imread(env_name, 'EXR-FI')
        else:
            env_name = os.path.join(self.root_dir, "env_maps_ldr", self.img_list[index] + ".png")
            env_map = im.imread(env_name)
        env_map = cv2.resize(env_map, (self.width,int(self.width/2)), interpolation=cv2.INTER_CUBIC)
        io_env_map = np.zeros_like(env_map)
        io_env_map[...,int(self.width/2):,:] = env_map[...,:int(self.width/2),:]
        io_env_map[...,:int(self.width/2),:] = env_map[...,int(self.width/2):,:]
        if self.log > 2:
            hdr_means[1] = np.percentile(io_env_map,50)
        else:
            hdr_means[1] = np.mean(io_env_map)
        io_env_map_t = self.to_tensor(io_env_map)
        if self.LDR:
            io_env_map_t = torch.mul(io_env_map_t,2) - torch.ones_like(io_env_map_t)
        if self.log and (self.LDR == 0):
            io_env_map_t = self.log_norm_t(io_env_map_t, self.log)

        # Generate Mask based on current crop value or load
        # Needed for env and edge networks
        if self.mask_step < 0: self.mask_step=0
        mask = self.load_mask(img_gray,self.mask_angle[self.mask_step])

        # load edge
        edge = self.load_edge(img_gray, index, mask)

        #load LDR
        ldr_name = os.path.join(self.root_dir, "ldr_images", self.img_list[index] + ".png")
        ldr = im.imread(ldr_name)
        ldr = cv2.resize(ldr, (self.width,int(self.width/2)), interpolation=cv2.INTER_CUBIC)
        #input to network
        in_out_ldr = np.zeros_like(ldr)
        in_out_ldr[...,int(self.width/2):,:] = ldr[...,:int(self.width/2),:]
        in_out_ldr[...,:int(self.width/2),:] = ldr[...,int(self.width/2):,:]
        in_out_ldr_gray = rgb2gray(in_out_ldr)
        io_ldr_t = self.to_tensor(in_out_ldr)
#        io_ldr_t = torch.mul(io_ldr_t,2) - torch.ones_like(io_ldr_t)

#        return torch.from_numpy(in_out_img), torch.from_numpy(img_gray), self.to_tensor(edge), self.to_tensor(mask), torch.from_numpy(io_env_map)
        return io_img_t, io_ldr_t, self.to_tensor(edge), self.to_tensor(mask), io_env_map_t, self.to_tensor(in_out_ldr_gray), torch.from_numpy(hdr_means).float(), self.to_tensor(in_out_img)

    def load_edge(self, img, index, mask):
        sigma = self.sigma

        # in test mode images are masked (with masked regions),
        # using 'mask' parameter prevents canny to detect edges for the masked regions
        mask = None if self.training else (1 - mask / 255).astype(np.bool)

        # canny
        if self.edge == 1:
            # no edge
            if sigma == -1:
                return np.zeros(img.shape).astype(np.float)

            # random sigma
            if sigma == 0:
                sigma = random.randint(1, 4)

            return canny(img, sigma=sigma, mask=mask).astype(np.float)

        # external
        else:
            img_name = os.path.join(self.root_dir, "edge_maps", self.img_list[index] + ".png")
            edge = im.imread(img_name)
            #edge = cv2.resize(edge, (self.width,int(self.width/2)), interpolation=cv2.INTER_CUBIC)
            edge = (edge > 0).astype(np.uint8) * 255

            # non-max suppression
            if self.nms == 1:
                edge = edge * canny(img, sigma=sigma, mask=mask)

            #in-out
            io_edge = np.zeros_like(edge)
            io_edge[...,int(self.width/2):] = edge[...,:int(self.width/2)]
            io_edge[...,:int(self.width/2)] = edge[...,int(self.width/2):]

            return io_edge

    def load_mask(self, img, mask_angle):
        imgh, imgw = img.shape[0:2]

        #generate mask based on FOV
        if self.mask == 0:
            gen_mask(imgh,imgw,mask_angle)

        #load mask
        if self.mask == 1:
            img_name = os.path.join(self.root_dir, "masks", "mask_"+ str(int(mask_angle)) + ".png")
            mask = im.imread(img_name)
            mask = cv2.resize(mask, (self.width,int(self.width/2)), interpolation=cv2.INTER_CUBIC)
            #mask = rgb2gray(mask)
            mask = (mask > 0).astype(np.uint8) * 255
            return mask

        if self.mask == 2:
            img_name = os.path.join(self.root_dir, "masks", "mask_60.png")
            mask = im.imread(img_name)
            mask = cv2.resize(mask, (self.width,int(self.width/2)), interpolation=cv2.INTER_CUBIC)
            mask = (mask > 0).astype(np.uint8) * 255
            return mask


    def to_tensor(self, img):
        #img = Image.fromarray(img)
        img_t = F.to_tensor(img).float()
        return img_t

    def log_norm(self, img, log_type):
        img = img*0.2
        img_1 = img+np.ones_like(img)
        if log_type == 1:
            hdr = np.where(img_1 < 1, 1, img_1)
            return np.clip(np.log(hdr)-1,-1,1)
        else:
            hdr_pos = np.where(img_1 < 1, 1, img_1)
            img_1n = np.negative(img)+np.ones_like(img)
            hdr_neg = np.where(img_1n < 1, 1, img_1n)
            return np.log(hdr_pos) - np.log(hdr_neg)

    def log_norm_t(self, img, log_type):
        ones = torch.ones_like(img)
        if log_type < 3:
#            img = torch.mul(img,(0.2*torch.mean(img)))
            img = torch.mul(img, self.alpha)
            img_1 = img+ones
            hdr_pos = torch.where(img_1 < 1, torch.ones(1).float(), img_1)
        if log_type == 1:
            return torch.clip(torch.log(hdr_pos)-ones,-1,1)
        elif log_type==2:
            img_1n = torch.negative(img)+ones
            hdr_neg = torch.where(img_1n < 1, torch.ones(1).float(), img_1n)
            return torch.log(hdr_pos) - torch.log(hdr_neg)
        elif log_type == 3:
#            img_pos = torch.where(img < 0, torch.zeros(1).float(), img)
            return gamma_correct_torch(img)
        elif log_type == 4:
#            img_pos = torch.where(img < 0, torch.zeros(1).float(), img)
            g_img = gamma_correct_torch(img,alpha=1)
            q = torch.quantile(g_img,0.5)
            return torch.mul(0.5/q,g_img)
        elif log_type == 5:
            return gamma_correct_torch(img) - ones
        elif log_type == 6:
            q = torch.quantile(img,0.5)
            g_img = gamma_correct_torch(img,gamma=2.4,alpha=0.5/q)
            return g_img - ones
        elif log_type == 7:
            return gamma_correct_torch(img,gamma=self.gamma,alpha=1) - ones
        elif log_type == 8:
            img_1 = img+ones
            hdr_pos = torch.where(img_1 < 1, ones, img_1)
            return torch.log(hdr_pos)
        elif log_type == 9:
            ldr = torch.where(img > 1, ones, img)
            hdr = torch.where(img <= 1, ones, img)
            hdr_log = torch.log(hdr)-ones
            ldr_norm = torch.mul(ldr,2)-ones
            return torch.cat((ldr_norm,hdr_log),dim=0)


    def create_iterator(self, batch_size):
        while True:
            sample_loader = DataLoader(
                dataset=self,
                batch_size=batch_size,
                drop_last=True
            )

            for item in sample_loader:
                yield item
