import os
import numpy as np
import matplotlib.pyplot as plt
import imageio as im
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
from .dataset import Dataset
from .models import EdgeModel, EnvModel, OutpaintingModel
from helpers.metrics import PSNR, EdgeAccuracy, angular_error, my_FID, RRA
#from skimage.metrics import structural_similarity as ssim
from torch.nn.parallel import DistributedDataParallel as DDP
import torch.distributed as dist
import helpers.dataset_processing as data
from helpers.dataset_processing import gamma_correct_torch, gamma_correct

from pytorch_msssim import ssim

#from ignite.metrics import *
#from ignite.engine import *

import time

"""
	Overall Model Class

	To Do:
	- add ability to use pretrained network modules
		-  use a model attribute to load it if the arg is true of pretraining it
	- check metrics and get angular error working efficiently

"""

class IllOutPainting():
	def __init__(self, args, device):
		self.args = args
		self.device = device
		self.start_epoch = 0
		self.gen_data = 'None'

		#Choose nad load models
		if args.model == 1:
			model_name = 'edge'
			self.edge_model = EdgeModel(args,device).to(device)
		elif args.model == 2:
			model_name = 'env_map'
			self.env_model = EnvModel(args,device).to(device)
		elif args.model == 3:
			model_name = 'outpaint'
			self.outpaint_model = OutpaintingModel(args,device).to(device)
		elif args.model == 4:
			model_name = 'env_out'
			self.env_model = EnvModel(args,device).to(device)
			self.outpaint_model = OutpaintingModel(args,device).to(device)
		elif args.model == 5:
			model_name = 'edge_out'
			self.edge_model = EdgeModel(args,device).to(device)
			self.outpaint_model = OutpaintingModel(args,device).to(device)
		elif args.model == 6:
			model_name = 'edge_env_out'
			self.edge_model = EdgeModel(args,device).to(device)
			self.env_model = EnvModel(args,device).to(device)
			self.outpaint_model = OutpaintingModel(args,device).to(device)

		#Load Models
		self.model_name = model_name
		#self.edge_model = DDP(EdgeModel(args,device),device)
		#self.env_model = DDP(EnvModel(args,device),device)
		#self.outpaint_model = DDP(OutpaintingModel(args,device),device)

		#add metrics
		self.psnr = PSNR(255.0).to(device)
		self.edgeacc = EdgeAccuracy(args.EDGE_THRESHOLD).to(device)
		self.fid = my_FID(device)
		self.ang_err = angular_error()
		self.RRA = RRA()
		self.L2_loss = nn.MSELoss()
		#self.SSIM_evaluator = Engine(self.batch_step)

		if self.args.mode == 4:
			self.test_dataset = Dataset(args, args.path+args.dataset_dir+"/test_style.csv", training=False)
		else:
			if self.args.mode == 2:
				self.test_dataset = Dataset(args, args.path+args.dataset_dir+"/test.csv", training=False)
				print("Test Set: ", len(self.test_dataset))

			self.train_dataset = Dataset(args, args.path+args.dataset_dir+"/train.csv", training=True)
			self.val_dataset = Dataset(args, args.path+args.dataset_dir+"/val.csv", training=True)
			#self.sample_iterator = self.val_dataset.create_iterator(config.SAMPLE_SIZE)

			print("Train Set: ", len(self.train_dataset))
			print("Val Set: ", len(self.val_dataset))

			self.start_mask = self.train_dataset.mask_step


	def load(self):
		if self.args.model == 2 or self.args.model == 4:
			self.env_model.load()
			if self.env_model.start_epoch:
				self.start_epoch = self.env_model.start_epoch

		if self.args.model == 1 or self.args.model == 5:
			self.edge_model.load()
			if self.edge_model.start_epoch:
				self.start_epoch = self.edge_model.start_epoch

		if self.args.model == 3:
			self.outpaint_model.load()
			if self.outpaint_model.start_epoch:
				self.start_epoch = self.outpaint_model.start_epoch

		if self.args.model == 6:
			self.env_model.load()
			self.edge_model.load()
			self.outpaint_model.load()


#		print("\nDEBUG!! start epoch: ",self.start_epoch)

	def checkpoint(self):
		if self.args.model == 2 or self.args.model == 4:
			self.gen_data = self.env_model.checkpoint()
			if self.env_model.start_epoch:
				self.start_epoch = self.env_model.start_epoch
			if self.env_model.start_mask > -1:
				self.start_mask = self.env_model.start_mask

		if self.args.model == 1 or self.args.model == 5:
			self.gen_data = self.edge_model.checkpoint()
			if self.edge_model.start_epoch:
				self.start_epoch = self.edge_model.start_epoch
			if self.edge_model.start_mask > -1:
				self.start_mask = self.edge_model.start_mask

		if self.args.model >= 3:
			self.gen_data = self.outpaint_model.checkpoint()
			if self.outpaint_model.start_epoch:
				self.start_epoch = self.outpaint_model.start_epoch
			if self.outpaint_model.start_mask > -1:
				self.start_mask = self.outpaint_model.start_mask

#		print("\nDEBUG!! start epoch: ",self.start_epoch)
#		print("\nDEBUG!! start mask: ",self.start_mask)

	def save(self, epoch, mask, val=False,gen_data='None'):
		if self.args.model == 2 or self.args.model == 4:
			self.env_model.save(epoch, mask, val,gen_data)

		if self.args.model == 1 or self.args.model == 5:
			self.edge_model.save(epoch, mask, val,gen_data)

		if self.args.model == 3 or self.args.model == 4 or self.args.model == 5:
			self.outpaint_model.save(epoch, mask, val,gen_data)

		if self.args.model == 6:
#			self.edge_model.save(epoch, mask, val,gen_data)
#			self.env_model.save(epoch, mask, val,gen_data)
			self.outpaint_model.save(epoch, mask, val,gen_data)

	#add graphing
	#finish
	def train(self):
		train_loader = DataLoader(
			dataset=self.train_dataset,
			batch_size=self.args.batch_size,
			#num_workers=4,
			drop_last=True,
			shuffle=True
		)

		epoch = 0
		keep_training = True
		model = self.args.model
		epochs = self.args.epochs
		total = len(self.train_dataset)
		if total == 0:
			print("No training data provided!")
			return

		self.train_dataset.mask_step = self.start_mask

		#plotting metrics and losses:
		#metrics:
		precision = 0.0
		recall = 0.0

		SSIM = 0.0
		psnr = 0.0
		fid = 0.0
		rra = [0.0, 0.0, 0.0]
		ae = 0.0

		e_SSIM = 0.0
		e_psnr = 0.0
		e_fid = 0.0
		e_ae = 0.0

		o_SSIM = 0.0
		o_psnr = 0.0
		o_fid = 0.0
		o_rra = [0.0, 0.0, 0.0]
		o_ae = 0.0

		#mask increase tracking
		mask_change = []
                #validaiton loss saving for pretrains
		val_max = 10
		over_count = 0
		#training
		tot_gen_loss = np.zeros((6,epochs))
		tot_dis_loss = np.zeros(epochs)

		if type(self.gen_data) != str:
			tot_gen_loss[:,:self.gen_data.shape[1]] = self.gen_data

		#validation
		val_gen_loss = np.zeros(epochs)
		val_dis_loss = np.zeros(epochs)

		val_o_gen_loss = np.zeros(epochs)
		val_o_dis_loss = np.zeros(epochs)

		val_env_gen_loss = np.zeros(epochs)
		val_env_dis_loss = np.zeros(epochs)

		val_edge_gen_loss = np.zeros(epochs)
		val_edge_dis_loss = np.zeros(epochs)

		len_train = len(train_loader)

		print("DEBUG!! start epoch: ", self.start_epoch)
		for epoch in range(self.start_epoch,epochs):
			logs=[]

			if self.args.model == 2 or self.args.model == 4:
				self.env_model.train()

			if self.args.model == 1 or self.args.model == 5:
				self.edge_model.train()

			if self.args.model == 3 or self.args.model == 4 or self.args.model == 5:
				self.outpaint_model.train()

			if self.args.model == 6:
				self.edge_model.eval()
				self.env_model.eval()
				self.outpaint_model.train()
				
			for i, data in enumerate(train_loader):

				batch_length = len(data[0])

				images, images_ldr, edges, masks, env_maps, ldr_img, hdr_means, hdr_true = self.cuda(data, self.device)
				if self.args.LDR:
					images = images_ldr

				#just edge model
				if model == 1:
					#train
					outputs, edge_gen_loss, edge_dis_loss, logs = self.edge_model.process(ldr_img, edges, masks)

					#backward
					self.edge_model.backward(edge_gen_loss, edge_dis_loss)

					#stats
					tot_gen_loss[0][epoch] += edge_gen_loss
					tot_gen_loss[1][epoch] += float(logs[1][1])
					tot_gen_loss[2][epoch] += float(logs[2][1])
					tot_gen_loss[3][epoch] += float(logs[3][1])
					tot_dis_loss[epoch] += edge_dis_loss

					logs.append(("edge_gen_loss",edge_gen_loss.item()))
					logs.append(("edge_dis_loss",edge_dis_loss.item()))


				#just env map model
				elif model == 2:
					#train
#					#DEBUG!!!
#					mask = torch.zeros_like(masks)
					if self.args.L2HDR:
						outputs, env_gen_loss, env_dis_loss, logs = self.env_model.process(images_ldr, env_maps, masks)
					else:
						outputs, env_gen_loss, env_dis_loss, logs = self.env_model.process(images, env_maps, masks)

					#backward
					self.env_model.backward(env_gen_loss, env_dis_loss)

					#stats
					tot_gen_loss[0][epoch] += env_gen_loss
					tot_gen_loss[1][epoch] += float(logs[1][1])
					tot_gen_loss[2][epoch] += float(logs[2][1])
					tot_gen_loss[3][epoch] += float(logs[3][1])
					tot_gen_loss[4][epoch] += float(logs[4][1])
					tot_gen_loss[5][epoch] += float(logs[5][1])
					tot_dis_loss[epoch] += env_dis_loss

					logs.append(("env_gen_loss",env_gen_loss.item()))
					logs.append(("env_dis_loss",env_dis_loss.item()))

				#just outpaint model 
				elif model == 3:
					# train
					outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images, edges, masks, env_maps, images_ldr, epoch+1)
					outputs_merged = (outputs * masks) + (images * (1 - masks))

					# backward
					self.outpaint_model.backward(o_gen_loss, o_dis_loss)

					#stats
					tot_gen_loss[0][epoch] += o_gen_loss
					tot_gen_loss[1][epoch] += float(logs[1][1])
					tot_gen_loss[2][epoch] += float(logs[2][1])
					tot_gen_loss[3][epoch] += float(logs[3][1])
					tot_gen_loss[4][epoch] += float(logs[4][1])
					tot_gen_loss[5][epoch] += float(logs[5][1])
					tot_dis_loss[epoch] += o_dis_loss

					logs.append(("O_gen_loss",o_gen_loss.item()))
					logs.append(("O_dis_loss",o_dis_loss.item()))
				elif model==6:
					with torch.no_grad():
						edge_outputs = self.edge_model.forward(ldr_img, edges, masks)
						env_outputs = self.env_model.forward(images, masks)
					outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images, edge_outputs, masks, env_outputs, images_ldr, epoch+1)
					outputs_merged = (outputs * masks) + (images * (1 - masks))

					# backward
					self.outpaint_model.backward(o_gen_loss, o_dis_loss)

					#stats
					tot_gen_loss[0][epoch] += o_gen_loss
					tot_gen_loss[1][epoch] += float(logs[1][1])
					tot_gen_loss[2][epoch] += float(logs[2][1])
					tot_gen_loss[3][epoch] += float(logs[3][1])
					tot_gen_loss[4][epoch] += float(logs[4][1])
					tot_gen_loss[5][epoch] += float(logs[5][1])
					tot_dis_loss[epoch] += o_dis_loss

					logs.append(("O_gen_loss",o_gen_loss.item()))
					logs.append(("O_dis_loss",o_dis_loss.item()))

				if (i+1) == len_train:
					#average logs for epoch
					log = []
					for l in range(1,tot_gen_loss.shape[0]):
						log.append((str(logs[l][0]),tot_gen_loss[l][epoch].item()/len_train ))
					log.append((str(logs[-2][0]), tot_gen_loss[0][epoch]/len_train ))
					log.append((str(logs[-1][0]), tot_dis_loss[epoch]/len_train ))

			#print Update
			#curr_batch = math.floor(((i+1)*batch_length*100)/total)
			#if curr_batch % 10 == 0 and last != curr_batch:
			#	last = curr_batch
#			print("E: {}/{}: {} ".format(epoch+1, self.args.epochs, logs))

			if (epoch + 1) % self.args.mask_epoch == 0:
				self.train_dataset.mask_step -= 1
				print("\nMask Step: ", self.train_dataset.mask_step)
			if (tot_gen_loss[0][epoch] / len(train_loader)) < self.args.mask_lim and (self.train_dataset.mask_step > 0):
				self.train_dataset.mask_step -= 1
				print("\nMask Step: ", self.train_dataset.mask_step)
				mask_change.append(epoch)
				over_count = 0
			if self.train_dataset.mask_step < 0: self.train_dataset.mask_step=0
			#save model
			if (epoch+1) % 10 == 0:
				print("Saving model at epoch: ", epoch+1)
				self.save(epoch, self.train_dataset.mask_step,gen_data=tot_gen_loss)
			if (epoch) % 5 == 0:
				print("\nPrinting Images...\n")
				self.print(epoch)

			#average logs for epoch
#			log = []
#			for l in range(1,6):
#				log.append((str(logs[l][0]),tot_gen_loss[l][epoch].item() / len(train_loader)))
		
			print("E: {}/{}: {} ".format(epoch+1, self.args.epochs, log))

			#evaluate
			#val_o_gen_loss[epoch], val_o_dis_loss[epoch], val_env_gen_loss[epoch], val_env_dis_loss[epoch], val_edge_gen_loss[epoch], val_edge_dis_loss[epoch] = self.eval(epoch)
			val_gen_loss[epoch], val_dis_loss[epoch] = self.eval(epoch)

			#save model at best validation loss
			if epoch > (epochs/2) and len(mask_change) < 1:
				if val_gen_loss[epoch] < val_max:
					self.save(epoch, self.train_dataset.mask_step, val =True)
					val_max = val_gen_loss[epoch]

			#go back to last save if model overfits
			if val_gen_loss[epoch] > self.args.overfit and model == 1 and self.args.mask_epoch > 50:
				self.checkpoint()
				epoch = self.start_epoch
				over_count += 1
				if over_count > 5:
					print("Model overfit too many times")
					return
				print("*** Model overfit, restting back to previous save ***")

		#final save of model
		self.save(epoch, self.train_dataset.mask_step, gen_data=tot_gen_loss)
		#test
		if self.args.mode == 2:
			self.test(epoch)
		#final print
		self.print(epoch)

		#average losses
		tot_gen_loss = tot_gen_loss / len(train_loader)
		tot_dis_loss = tot_dis_loss / len(train_loader)
		
		#plot losses for each module
		e = range(0,self.args.epochs,1)
		#plot env losses
		if self.args.model == 2 or self.args.model == 4 or self.args.model == 6:
			fig, ax = plt.subplots()
			ax.set_xlabel("Epoch")
			ax.set_ylabel("Loss")

			ax.plot(e,tot_gen_loss[0], color='r',label="Training Generator Loss")
			ax.plot(e,tot_gen_loss[1], color='c',linestyle='--',label="Training Adversarial Loss")
#			ax.plot(e,tot_gen_loss[2], color='m',linestyle='--',label="Training Feature Match Loss")
			ax.plot(e,tot_gen_loss[2], color='m',linestyle='--',label="Training Perceptual Loss")
			ax.plot(e,tot_gen_loss[3], color='g',linestyle='--',label="Training Content Loss")
			ax.plot(e,tot_gen_loss[4], color='b',linestyle='--',label="Training L1 Loss")
			ax.plot(e,tot_gen_loss[5], color='g',linestyle='--',label="Training L1 HDR Loss")
#			ax.plot(e,tot_dis_loss, color='y',label="Training Discriminator Loss")

			ax.plot(e,val_gen_loss, color='g',label="Validation Generator Loss")
			#ax.plot(e,val_dis_loss, color='b',label="Validation Discriminator Loss")
			ax.vlines(x = mask_change, ymin = 0, ymax = val_gen_loss.max(), colors = 'purple', label = 'Mask Changes')

			ax.legend(loc=0)
			plt.title("Environment Map Loss")
			plt.savefig(self.args.path + self.args.output_dir + "/env_map_losses_{}.png".format(self.args.name))

			plt.close()

		#plot edge losses
		if self.args.model == 1 or self.args.model >= 5:
			fig, ax = plt.subplots()
			ax.set_xlabel("Epoch")
			ax.set_ylabel("Loss")
		
			ax.plot(e,tot_gen_loss[0], color='r',label="Training Generator Loss")
			ax.plot(e,tot_gen_loss[1], color='c',linestyle='--',label="Training Adversarial Loss")
			ax.plot(e,tot_gen_loss[2], color='m',linestyle='--',label="Training Feature Match Loss")
#			ax.plot(e,tot_gen_loss[3], color='b',linestyle='--',label="Training L1 Loss")
			ax.plot(e,tot_dis_loss, color='y',label="Training Discriminator Loss")

			ax.plot(e,val_gen_loss, color='g',label="Validation Generator Loss")
#			ax.plot(e,val_dis_loss, color='b',label="Validation Discriminator Loss")
			ax.vlines(x = mask_change, ymin = 0, ymax = val_gen_loss.max(), colors = 'purple', label = 'Mask Changes')

			ax.legend(loc=0)
			plt.title("Edge Map Loss")
			plt.savefig(self.args.path + self.args.output_dir + "/edge_map_losses_{}.png".format(self.args.name))

			plt.close()

		#plot outpainting losses
		if self.args.model >= 3:
			fig, ax = plt.subplots()
			ax.set_xlabel("Epoch")
			ax.set_ylabel("Loss")
		
			ax.plot(e,tot_gen_loss[0], color='r',label="Training Generator Loss")
			ax.plot(e,tot_gen_loss[1], color='c',linestyle='--',label="Training Adversarial Loss")
			ax.plot(e,tot_gen_loss[2], color='m',linestyle='--',label="Training L1 Loss")
			ax.plot(e,tot_gen_loss[3], color='b',linestyle='--',label="Training Content Loss")
			ax.plot(e,tot_gen_loss[4], color='y',linestyle='--',label="Training Style Loss")
			ax.plot(e,tot_gen_loss[5], color='g',linestyle='--',label="Training Cosine Loss")
#			ax.plot(e,tot_dis_loss, color='y',label="Training Discriminator Loss")

			ax.plot(e,val_gen_loss, color='k',label="Validation Generator Loss")
#			ax.plot(e,val_dis_loss, color='b',label="Validation Discriminator Loss")
			ax.vlines(x = mask_change, ymin = 0, ymax = val_gen_loss.max(), colors = 'purple', label = 'Mask Changes')

			ax.legend(loc=0)
			plt.title("Outpainting Loss")
			plt.savefig(self.args.path + self.args.output_dir + "/outpainting_losses_{}.png".format(self.args.name))

			plt.close()


	def eval(self,epoch):
		val_loader = DataLoader(
			dataset=self.val_dataset,
			batch_size=self.args.batch_size,
			drop_last=True,
			shuffle=True
		)

		model = self.args.model
		total = len(self.val_dataset)
		if total == 0:
			print("No validation data provided!")
			return

		#metrics
		precision = 0.0
		recall = 0.0

		SSIM = 0.0
		psnr = 0.0
		fid = 0.0
		rra = [0.0, 0.0, 0.0]
		ae = 0.0
		rmse = 0.0

		e_SSIM = 0.0
		e_psnr = 0.0
		e_fid = 0.0
		e_ae = 0.0
		e_rmse = 0.0

		o_SSIM = 0.0
		o_psnr = 0.0
		o_fid = 0.0
		o_rra = [0.0, 0.0, 0.0]
		o_ae = 0.0
		o_rmse = 0.0

		gen_loss = 0
		dis_loss = 0

		out_gen_loss = 0
		out_dis_loss = 0
		env_gen_loss = 0
		env_dis_loss = 0
		edge_gen_loss = 0
		edge_dis_loss = 0


		e_gen_loss = 0
		e_dis_loss = 0
		o_gen_loss = 0
		o_dis_loss = 0

		if self.args.model == 2 or self.args.model == 4:
			self.env_model.eval()

		if self.args.model == 1 or self.args.model == 5:
			self.edge_model.eval()

		if self.args.model == 3 or self.args.model == 4 or self.args.model == 5:
			self.outpaint_model.eval()

		if self.args.model == 6:
			self.edge_model.eval()
			self.env_model.eval()
			self.outpaint_model.eval()

		with torch.no_grad():
			for i, data in enumerate(val_loader):

				batch_length = len(data[0])

				images, images_ldr, edges, masks, env_maps, ldr_img, hdr_means, hdr_true = self.cuda(data,self.device)
				if self.args.LDR:
					images = images_ldr

				#just edge model
				if model == 1:
					#train
					outputs, e_gen_loss, e_dis_loss, logs = self.edge_model.process(ldr_img, edges, masks)
					gen_loss += e_gen_loss
					dis_loss += e_dis_loss

					#metrics
					p, r = self.edgeacc(edges * masks, outputs * masks)
					precision += p
					recall += r

					if (i+1) == len(val_loader):
						precision /= (i+1)
						recall /= (i+1)

						logs.append(("Precision",precision))
						logs.append(("Recall",recall))

						gen_loss /= len(val_loader)
						dis_loss /= len(val_loader)

				#just env map model
				elif model == 2:
					#train
					if self.args.L2HDR:
						outputs, env_gen_loss, env_dis_loss, logs = self.env_model.process(images_ldr, env_maps, masks)
					else:
						outputs, e_gen_loss, e_dis_loss, logs = self.env_model.process(images, env_maps, masks)
					gen_loss += e_gen_loss
					dis_loss += e_dis_loss

					ones = torch.ones_like(env_maps)
					if self.args.log_norm == 9:
						env_maps_img = self.log_unorm(env_maps)
						outputs_img = self.log_unorm(outputs)
					elif self.args.log_norm == 7:
						env_maps_img = gamma_correct_torch(env_maps+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
						outputs_img = gamma_correct_torch(outputs+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
					else:
						env_maps_img = torch.div(torch.exp(env_maps+ones)-ones,self.args.alpha)-ones
						outputs_img = torch.div(torch.exp(outputs+ones)-ones,self.args.alpha)-ones				#metrics
					# RMSE?
					#ssim_images = env_maps.permute((0,2,3,1)).cpu().detach().numpy()
					#ssim_outputs = outputs.permute((0,2,3,1)).cpu().detach().numpy()
					#SSIM += ssim(ssim_images, ssim_outputs, data_range=ssim_outputs.max() - ssim_outputs.min(), multichannel=True)
					#SSIM += ssim(env_maps.permute((0,2,3,1)), outputs.permute((0,2,3,1)), data_range=outputs.max() - outputs.min(), multichannel=True)
					SSIM += torch.mean(ssim(env_maps_img, outputs_img, data_range=outputs.max()-outputs.min(), size_average=False))
					psnr += self.psnr(env_maps_img.permute((0,2,3,1)),outputs_img.permute((0,2,3,1)))
					if (epoch+1 == self.args.epochs):
						fid += self.fid(env_maps_img,outputs_img)
					#ae += self.ang_err(env_maps,outputs)
					rmse += torch.sqrt(self.L2_loss(env_maps_img,outputs_img))

					#average metrics
					if (i+1) == len(val_loader):
						SSIM /= (i+1)
						psnr /= (i+1)
						#ae /= (i+1)
						rmse /= (i+1)

						logs.append(("SSIM",SSIM.item()))
						logs.append(("PSNR",psnr.item()))
						if (epoch+1 == self.args.epochs):
							fid /= (i+1)
							logs.append(("FID",fid.item()))
						#logs.append(("ae",ae.item()))
						logs.append(("RMSE",rmse.item()))

						gen_loss /= len(val_loader)
						dis_loss /= len(val_loader)

				#just outpaint model 
				elif model == 3:
					# train
					outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images, edges, masks, env_maps,images_ldr,epoch+1)
					gen_loss += o_gen_loss
					dis_loss += o_dis_loss
					outputs_merged = (outputs * masks) + (images * (1 - masks))
					ones = torch.ones_like(images)
					if self.args.log_norm == 1:
						images_img = torch.div(torch.exp(images+ones),self.args.alpha)-ones
						outputs_img = torch.div(torch.exp(outputs_merged+ones),self.args.alpha)-ones
					elif self.args.log_norm == 7:
						images_img = gamma_correct_torch(images+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
						outputs_img = gamma_correct_torch(outputs+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
					else:
						images_img = torch.div(images+1,2)
						outputs_img = torch.div(outputs+1,2)

					# metrics
					#ssim_images = images.permute((0,2,3,1)).cpu().detach().numpy()
					#ssim_outputs = outputs_merged.permute((0,2,3,1)).cpu().detach().numpy()
					#SSIM += ssim(ssim_images, ssim_outputs, data_range=ssim_outputs.max() - ssim_outputs.min(), multichannel=True)
					SSIM += torch.mean(ssim(images_img, outputs_img, data_range=images_img.max()-images_img.min(), size_average=False))
					psnr += self.psnr(images_img.permute((0,2,3,1)),outputs_img.permute((0,2,3,1)))

					if (epoch+1 == self.args.epochs):
						fid += self.fid(images_img,outputs_img)
					#ae += self.ang_err(images,outputs_merged)
					red_rra, green_rra, blue_rra = self.RRA(images_img,outputs_img)
					rra[0] = red_rra
					rra[1] = green_rra
					rra[2] = blue_rra

					rmse += torch.sqrt(self.L2_loss(images_img.detach(),outputs_img.detach()))

					#average metrics
					if (i+1) == len(val_loader):
						SSIM /= (i+1)
						psnr /= (i+1)
						rra = [x/(i+1) for x in rra]
						#ae /= (i+1)
						rmse /= (i+1)

						logs.append(("SSIM",SSIM.item()))
						logs.append(("PSNR",psnr.item()))
						if (epoch+1 == self.args.epochs):
							fid /= (i+1)
							logs.append(("FID",fid.item()))
						logs.append(("rra",rra))
						logs.append(("RMSE",rmse.item()))
						#logs.append(("ae",ae.item()))

						gen_loss /= len(val_loader)
						dis_loss /= len(val_loader)

				elif model == 6:
					edge_outputs, e_gen_loss, e_dis_loss, logs = self.edge_model.process(ldr_img, edges, masks)
					edge_gen_loss += e_gen_loss
					edge_dis_loss += e_dis_loss

					env_outputs, e_gen_loss, e_dis_loss, logs = self.env_model.process(images, env_maps, masks)
					env_gen_loss += e_gen_loss
					env_dis_loss += e_dis_loss

					outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images, edge_outputs, masks, env_outputs, images_ldr, epoch+1)
					gen_loss += o_gen_loss
					dis_loss += o_dis_loss
					outputs_merged = (outputs * masks) + (images * (1 - masks))


					ones = torch.ones_like(images)
					if self.args.log_norm == 1:
						images_img = torch.div(torch.exp(images+ones),self.args.alpha)-ones
						env_maps_img = torch.div(torch.exp(env_maps+ones),self.args.alpha)-ones

						env_outputs_img = torch.div(torch.exp(env_outputs+ones),self.args.alpha)-ones
						outputs_img = torch.div(torch.exp(outputs_merged+ones),self.args.alpha)-ones

					elif self.args.log_norm == 7:
						images_img = gamma_correct_torch(images+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
						env_maps_img = gamma_correct_torch(env_maps+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)

						env_outputs_img = gamma_correct_torch(env_outputs+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
						outputs_img = gamma_correct_torch(outputs+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
					else:
						images_img = torch.div(images+ones,2)
						env_maps_img = torch.div(env_mapss+ones,2)

						env_outputs_img = torch.div(env_outputs+ones,2)
						outputs_img = torch.div(outputs+ones,2)


					#metrics
#					p, r = self.edgeacc(edges * masks, edge_outputs * masks)
#					precision += p
#					recall += r

					e_SSIM += torch.mean(ssim(env_maps.permute((0,2,3,1)), env_outputs.permute((0,2,3,1)), data_range=outputs.max() - outputs.min(), size_average=False))
					e_psnr += self.psnr(env_maps.permute((0,2,3,1)),env_outputs.permute((0,2,3,1)))
#					e_fid += self.fid(env_maps_img,outputs_img)
#					e_ae += self.ang_err(env_maps_img,outputs_img)
					e_rmse += torch.sqrt(self.L2_loss(env_maps_img.detach(),env_outputs_img.detach()))

					o_SSIM += torch.mean(ssim(images_img, outputs_img, data_range=images_img.max()-images_img.min(), size_average=False))
					o_psnr += self.psnr(images_img.permute((0,2,3,1)),outputs_img.permute((0,2,3,1)))
#					o_fid += self.fid(images_img,outputs_img)
#					o_ae += self.ang_err(images_img,outputs_img)
					red_rra, green_rra, blue_rra = self.RRA(images_img,outputs_img)
					o_rra[0] = red_rra
					o_rra[1] = green_rra
					o_rra[2] = blue_rra

					o_rmse += torch.sqrt(self.L2_loss(images_img.detach(),outputs_img.detach()))

					#average metrics
					if (i+1) == len(val_loader):
						precision /= (i+1)
						recall /= (i+1)

						e_SSIM /= (i+1)
						e_psnr /= (i+1)
#						e_fid /= (i+1)
#						e_ae /= (i+1)
						e_rmse /= (i+1)

						o_SSIM /= (i+1)
						o_psnr /= (i+1)
#						o_fid /= (i+1)
						o_rra = [x/(i+1) for x in o_rra]
#						o_ae /= (i+1)
						o_rmse /= (i+1)

						logs.append(("Precision",precision))
						logs.append(("Recall",recall))

						logs.append(("e_SSIM",e_SSIM.item()))
						logs.append(("e_PSNR",e_psnr.item()))
#						logs.append(("e_FID",e_fid.item()))
#						logs.append(("e_ae",e_ae.item()))
						logs.append(("e_RMSE",e_rmse.item()))

						logs.append(("o_SSIM",o_SSIM.item()))
						logs.append(("o_PSNR",o_psnr.item()))
#						logs.append(("o_FID",o_fid.item()))
						logs.append(("o_rra",o_rra))
#						logs.append(("o_ae",o_ae.item()))
						logs.append(("o_RMSE",o_rmse.item()))

						gen_loss /= len(val_loader)
						dis_loss /= len(val_loader)

				
		#print("Validating Epoch: Outpainting Loss: {}, {}. Env Map Loss: {}, {}. Edge Map Loss: {}, {}. Logs: {}".format(out_gen_loss, out_dis_loss, env_gen_loss, env_dis_loss, edge_gen_loss, edge_dis_loss, logs))
		#return out_gen_loss, out_dis_loss, env_gen_loss, env_dis_loss, edge_gen_loss, edge_dis_loss
		print("Validating Epoch: Gen: {}, Dis: {}. Logs: {}".format(gen_loss, dis_loss, logs))
		return gen_loss, dis_loss

	def test(self, epoch):
		test_loader = DataLoader(
			dataset=self.test_dataset,
			batch_size=self.args.batch_size,
			drop_last=True,
			shuffle=True
		)

		model = self.args.model
		total = len(self.test_dataset)
		if total == 0:
			print("No test data provided!")
			return

		#metrics
		precision = 0.0
		recall = 0.0

		SSIM = 0.0
		psnr = 0.0
		fid = 0.0
		rra = [0.0, 0.0, 0.0]
		ae = 0.0
		rmse = 0.0

		raw_SSIM = 0.0
		raw_psnr = 0.0
		raw_fid = 0.0
		raw_rra = [0.0, 0.0, 0.0]
		raw_ae = 0.0
		raw_rmse = 0.0

		e_SSIM = 0.0
		e_psnr = 0.0
		e_fid = 0.0
		e_ae = 0.0
		e_rmse = 0.0

		o_SSIM = 0.0
		o_psnr = 0.0
		o_fid = 0.0
		o_rra = [0.0, 0.0, 0.0]
		o_ae = 0.0
		o_rmse = 0.0

		out_gen_loss = 0
		out_dis_loss = 0

		e_gen_loss = 0
		e_dis_loss = 0
		o_gen_loss = 0
		o_dis_loss = 0

		env_gen_loss = 0
		env_dis_loss = 0

		edge_gen_loss = 0
		edge_dis_loss = 0

		if self.args.model == 2 or self.args.model == 4:
			self.env_model.eval()

		if self.args.model == 1 or self.args.model == 5:
			self.edge_model.eval()

		if self.args.model == 3 or self.args.model == 4 or self.args.model == 5:
			self.outpaint_model.eval()

		if self.args.model == 6:
			self.edge_model.eval()
			self.env_model.eval()
			self.outpaint_model.eval()

		with torch.no_grad():
			for i, data in enumerate(test_loader):

				batch_length = len(data[0])

				images, images_ldr, edges, masks, env_maps, ldr_img, hdr_means, hdr_true = self.cuda(data,self.device)
				if self.args.LDR:
					images = images_ldr

				#just edge model
				if model == 1:
					#train
					outputs, e_gen_loss, e_dis_loss, logs = self.edge_model.process(ldr_img, edges, masks)
					edge_gen_loss += e_gen_loss
					edge_dis_loss += e_dis_loss

					#metrics
					p, r = self.edgeacc(edges * masks, outputs * masks)
					precision += p
					recall += r

					if (i+1) == len(test_loader):
						precision /= (i+1)
						recall /= (i+1)

						logs.append(("Precision",precision))
						logs.append(("Recall",recall))

						edge_gen_loss /= len(test_loader)
						edge_dis_loss /= len(test_loader)

				#just env map model
				elif model == 2:
					if self.args.L2HDR:
						outputs, e_gen_loss, e_dis_loss, logs = self.env_model.process(images_ldr, env_maps, masks)
					else:
						outputs, env_gen_loss, env_dis_loss, logs = self.env_model.process(images, env_maps, masks)

					env_gen_loss += e_gen_loss
					env_dis_loss += e_dis_loss

					ones = torch.ones_like(env_maps)
					if self.args.log_norm == 9:
						env_maps_img = self.log_unorm(env_maps)
						outputs_img = self.log_unorm(outputs)
					elif self.args.log_norm == 7:
						env_maps_img = gamma_correct_torch(env_maps+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
						outputs_img = gamma_correct_torch(outputs+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
					else:
						env_maps_img = torch.div(torch.exp(env_maps+ones)-ones,self.args.alpha)-ones
						outputs_img = torch.div(torch.exp(outputs+ones)-ones,self.args.alpha)-ones
					#metrics
					# RMSE?
					#ssim_images = env_maps.permute((0,2,3,1)).cpu().detach().numpy()
					#ssim_outputs = outputs.permute((0,2,3,1)).cpu().detach().numpy()
					#SSIM += ssim(ssim_images, ssim_outputs, data_range=ssim_outputs.max() - ssim_outputs.min(), multichannel=True)
					SSIM += torch.mean(ssim(env_maps_img, outputs_img, data_range=outputs.max()-outputs.min(), size_average=False))
					#SSIM += ssim(env_maps.permute((0,2,3,1)), outputs.permute((0,2,3,1)), data_range=outputs.max() - outputs.min(), multichannel=True)
					psnr += self.psnr(env_maps_img.permute((0,2,3,1)),outputs_img.permute((0,2,3,1)))
					fid += self.fid(env_maps_img,outputs_img)
					ae += self.ang_err(env_maps_img,outputs_img)
					rmse += torch.sqrt(self.L2_loss(env_maps_img,outputs_img))

					#average metrics
					if (i+1) == len(test_loader):
						SSIM /= (i+1)
						psnr /= (i+1)
						fid /= (i+1)
						ae /= (i+1)
						rmse /= (i+1)

						logs.append(("SSIM",SSIM.item()))
						logs.append(("PSNR",psnr.item()))
						logs.append(("FID",fid.item()))
						logs.append(("ae",ae))
						logs.append(("RMSE",rmse.item()))

						env_gen_loss /= len(test_loader)
						env_dis_loss /= len(test_loader)

				#just outpaint model 
				elif model == 3:
					# train
					outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images, edges, masks, env_maps, images_ldr, epoch+1)
					out_gen_loss += o_gen_loss
					out_dis_loss += o_dis_loss
					outputs_merged = (outputs * masks) + (images * (1 - masks))
					ones = torch.ones_like(images)
					if self.args.log_norm == 1:
						images_img = torch.div(torch.exp(images+ones),self.args.alpha)-ones
						outputs_img = torch.div(torch.exp(outputs_merged+ones),self.args.alpha)-ones
					elif self.args.log_norm == 7:
						images_img = gamma_correct_torch(images+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
						outputs_img = gamma_correct_torch(outputs+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
					else:
						images_img = torch.div(images+ones,2)
						outputs_img = torch.div(outputs+ones,2)


					# metrics
					#ssim_images = images.permute((0,2,3,1)).cpu().detach().numpy()
					#ssim_outputs = outputs_merged.permute((0,2,3,1)).cpu().detach().numpy()
					#SSIM += ssim(ssim_images, ssim_outputs, data_range=ssim_outputs.max() - ssim_outputs.min(), multichannel=True)
					SSIM += torch.mean(ssim(images_img, outputs_img, data_range=images_img.max()-images_img.min(), size_average=False))
					psnr += self.psnr(images_img.permute((0,2,3,1)),outputs_img.permute((0,2,3,1)))
					fid += self.fid(images_img,outputs_img)
					ae += self.ang_err(images_img,outputs_img)
					red_rra, green_rra, blue_rra = self.RRA(images_img,outputs_img)
					rra[0] = red_rra
					rra[1] = green_rra
					rra[2] = blue_rra

					rmse += torch.sqrt(self.L2_loss(images_img.detach(),outputs_img.detach()))

					#compare outputs to raw unclipped inputs
					if epoch == self.args.epochs:
						hdr_raw = hdr_true
						raw_SSIM += torch.mean(ssim(hdr_raw, outputs_img, data_range=outputs_img.max()-outputs_img.min(), size_average=False))
						raw_psnr += self.psnr(hdr_raw.permute((0,2,3,1)),outputs_img.permute((0,2,3,1)))
						raw_fid += self.fid(hdr_raw,outputs_img)
						raw_ae += self.ang_err(hdr_raw,outputs_img)
						red_rra, green_rra, blue_rra = self.RRA(hdr_raw,outputs_img)
						raw_rra[0] = red_rra
						raw_rra[1] = green_rra
						raw_rra[2] = blue_rra

						raw_rmse += torch.sqrt(nn.MSELoss(hdr_raw.detach(),outputs_img.detach()))


					#average metrics
					if (i+1) == len(test_loader):
						SSIM /= (i+1)
						psnr /= (i+1)
						fid /= (i+1)
						rra = [x/(i+1) for x in rra]
						ae /= (i+1)
						rmse /= (i+1)

						logs.append(("SSIM",SSIM.item()))
						logs.append(("PSNR",psnr.item()))
						logs.append(("FID",fid.item()))
						logs.append(("rra",rra))
						logs.append(("ae",ae.item()))
						logs.append(("RMSE",rmse.item()))

						out_gen_loss /= len(test_loader)
						out_dis_loss /= len(test_loader)

					
				# env and outpaint
				elif model == 4:
					#train
					env_outputs, env_gen_loss, env_dis_loss, logs = self.env_model.process(images, env_maps, masks)
					env_outputs = env_outputs * masks + env_maps * (1 - masks)
					outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images, edges, masks, env_outputs,epoch+1)
					outputs_merged = (outputs * masks) + (images * (1 - masks))

					#metrics
					# RMSE?
					e_SSIM += ssim(env_maps.permute((0,2,3,1)), outputs.permute((0,2,3,1)), data_range=outputs.max() - outputs.min(), multichannel=True)
					e_psnr += self.psnr(env_maps.permute((0,2,3,1)),outputs.permute((0,2,3,1)))
					e_fid += self.fid(env_maps,outputs)
					e_ae += self.ang_err(env_maps,outputs)

					o_SSIM += ssim(images.permute((0,2,3,1)), outputs_merged.permute((0,2,3,1)), data_range=outputs.max() - outputs.min(), multichannel=True)
					o_psnr += self.psnr(images.permute((0,2,3,1)),outputs_merged.permute((0,2,3,1)))
					o_fid += self.fid(images,outputs_merged)
					o_ae += self.ang_err(images,outputs_merged)
					o_rra += self.RRA(images,outputs_merged)

					#average metrics
					if (i+1) == len(test_loader):
						e_SSIM /= (i+1)
						e_psnr /= (i+1)
						e_fid /= (i+1)
						e_ae /= (i+1)

						o_SSIM /= (i+1)
						o_psnr /= (i+1)
						o_fid /= (i+1)
						o_rra /= (i+1)
						o_ae /= (i+1)

						logs.append(("e_SSIM",e_SSIM.item()))
						logs.append(("e_PSNR",e_psnr.item()))
						logs.append(("e_FID",e_fid.item()))
						logs.append(("e_ae",e_ae.item()))

						logs.append(("o_SSIM",o_SSIM.item()))
						logs.append(("o_PSNR",o_psnr.item()))
						logs.append(("o_FID",o_fid.item()))
						logs.append(("o_rra",o_rra.item()))
						logs.append(("o_ae",o_ae.item()))

				#edge and outpaint
				elif model == 5:
					edge_outputs, edge_gen_loss, edge_dis_loss, logs = self.edge_model.process(images_gray, edges, masks)
					edge_outputs = edge_outputs * masks + edges * (1 - masks)
					outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images, edge_ouputs, masks, env_maps,epoch+1)
					outputs_merged = (outputs * masks) + (images * (1 - masks))

					#metrics
					p, r = self.edgeacc(edges * masks, outputs * masks)
					precision += p
					recall += r

					o_SSIM += ssim(images.permute((0,2,3,1)), outputs_merged.permute((0,2,3,1)), data_range=outputs.max() - outputs.min(), multichannel=True)
					o_psnr += self.psnr(images.permute((0,2,3,1)),outputs_merged.permute((0,2,3,1)))
					o_fid += self.fid(images,outputs_merged)
					o_ae += self.ang_err(images,outputs_merged)
					o_rra += self.RRA(images,outputs_merged)

					#average metrics
					if (i+1) == len(test_loader):
						precision /= (i+1)
						recall /= (i+1)

						o_SSIM /= (i+1)
						o_psnr /= (i+1)
						o_fid /= (i+1)
						o_rra /= (i+1)
						o_ae /= (i+1)

						logs.append(("Precision",precision.item()))
						logs.append(("Recall",recall.item()))

						logs.append(("o_SSIM",o_SSIM.item()))
						logs.append(("o_PSNR",o_psnr.item()))
						logs.append(("o_FID",o_fid.item()))
						logs.append(("o_rra",o_rra.item()))
						logs.append(("o_ae",o_ae.item()))

				#all models
				else:
					edge_outputs, edge_gen_loss, edge_dis_loss, logs = self.edge_model.process(ldr_img, edges, masks)
					edge_gen_loss += e_gen_loss
					edge_dis_loss += e_dis_loss

					env_outputs, e_gen_loss, e_dis_loss, logs = self.env_model.process(images, env_maps, masks)
					env_gen_loss += e_gen_loss
					env_dis_loss += e_dis_loss

					outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images, edge_outputs, masks, env_outputs, images_ldr, epoch+1)
					out_gen_loss += o_gen_loss
					out_dis_loss += o_dis_loss
					outputs_merged = (outputs * masks) + (images * (1 - masks))


					ones = torch.ones_like(images)
					if self.args.log_norm == 1:
						images_img = torch.div(torch.exp(images+ones),self.args.alpha)-ones
						env_maps_img = torch.div(torch.exp(env_maps+ones),self.args.alpha)-ones

						env_outputs_img = torch.div(torch.exp(env_outputs+ones),self.args.alpha)-ones
						outputs_img = torch.div(torch.exp(outputs_merged+ones),self.args.alpha)-ones

					elif self.args.log_norm == 7:
						images_img = gamma_correct_torch(images+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
						env_maps_img = gamma_correct_torch(env_maps+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)

						env_outputs_img = gamma_correct_torch(env_outputs+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
						outputs_img = gamma_correct_torch(outputs+ones,gamma=self.args.gamma,inverse=1,alpha=1,device=self.device)
					else:
						images_img = torch.div(images+ones,2)
						env_maps_img = torch.div(env_mapss+ones,2)

						env_outputs_img = torch.div(env_outputs+ones,2)
						outputs_img = torch.div(outputs+ones,2)


					#metrics
#					p, r = self.edgeacc(edges * masks, edge_outputs * masks)
#					precision += p
#					recall += r

					e_SSIM += torch.mean(ssim(env_maps.permute((0,2,3,1)), env_outputs.permute((0,2,3,1)), data_range=outputs.max() - outputs.min(), size_average=False))
					e_psnr += self.psnr(env_maps.permute((0,2,3,1)),env_outputs.permute((0,2,3,1)))
					e_fid += self.fid(env_maps_img,outputs_img)
					e_ae += self.ang_err(env_maps_img,outputs_img)
					e_rmse += torch.sqrt(self.L2_loss(env_maps_img.detach(),env_outputs_img.detach()))

					o_SSIM += torch.mean(ssim(images_img, outputs_img, data_range=images_img.max()-images_img.min(), size_average=False))
					o_psnr += self.psnr(images_img.permute((0,2,3,1)),outputs_img.permute((0,2,3,1)))
					o_fid += self.fid(images_img,outputs_img)
					o_ae += self.ang_err(images_img,outputs_img)
					red_rra, green_rra, blue_rra = self.RRA(images_img,outputs_img)
					o_rra[0] = red_rra
					o_rra[1] = green_rra
					o_rra[2] = blue_rra

					o_rmse += torch.sqrt(self.L2_loss(images_img.detach(),outputs_img.detach()))

					#average metrics
					if (i+1) == len(test_loader):
						precision /= (i+1)
						recall /= (i+1)

						e_SSIM /= (i+1)
						e_psnr /= (i+1)
						e_fid /= (i+1)
						e_ae /= (i+1)
						e_rmse /= (i+1)

						o_SSIM /= (i+1)
						o_psnr /= (i+1)
						o_fid /= (i+1)
						o_rra = [x/(i+1) for x in o_rra]
						o_ae /= (i+1)
						o_rmse /= (i+1)

						logs.append(("Precision",precision))
						logs.append(("Recall",recall))

						logs.append(("e_SSIM",e_SSIM.item()))
						logs.append(("e_PSNR",e_psnr.item()))
						logs.append(("e_FID",e_fid.item()))
						logs.append(("e_ae",e_ae.item()))
						logs.append(("e_RMSE",e_rmse.item()))

						logs.append(("o_SSIM",o_SSIM.item()))
						logs.append(("o_PSNR",o_psnr.item()))
						logs.append(("o_FID",o_fid.item()))
						logs.append(("o_rra",o_rra))
						logs.append(("o_ae",o_ae.item()))
						logs.append(("o_RMSE",o_rmse.item()))

						out_gen_loss /= len(test_loader)
						out_dis_loss /= len(test_loader)



		#print all loss values
		print("Test: (Gen, Disc) Outpainting Loss: {}, {}. Env Map Loss: {}, {}. Edge Map Loss: {}, {}. Logs: {}".format(out_gen_loss, out_dis_loss, env_gen_loss, env_dis_loss, edge_gen_loss, edge_dis_loss, logs))
		return out_gen_loss, out_dis_loss, env_gen_loss, env_dis_loss, edge_gen_loss, edge_dis_loss
		
	def print(self, epoch):
		mode = self.args.mode
		bs = 16
		if mode == 0:
			#use train_set
			loader = DataLoader(
				dataset=self.train_dataset,
				batch_size=self.args.batch_size,
				drop_last=True,
				shuffle=True
			)
		elif mode == 1:
			#use val set
			loader = DataLoader(
				dataset=self.val_dataset,
				batch_size=self.args.batch_size,
				drop_last=True,
				shuffle=True
			)
		else:
			self.test_dataset.mask_step = self.train_dataset.mask_step
			#use test set
			loader = DataLoader(
				dataset=self.test_dataset,
				batch_size=self.args.batch_size,
				drop_last=True,
				shuffle=True
			)
		model = self.args.model
		log_alpha = self.args.log_alpha

		with torch.no_grad():
			dataiter = iter(loader)
#			hdr_means = np.array(2)
			images, images_ldr, edges, masks, env_maps, ldr_img, hdr_means, hdr_true = self.cuda(dataiter.next(),self.device)
			hdr_means = hdr_means.cpu().detach().numpy()
			#just edge model
			if model == 1:
				#train
				outputs, edge_gen_loss, edge_dis_loss, logs = self.edge_model.process(ldr_img, edges, masks)
				p_out = outputs.permute((0,2,3,1))
				p_edges = edges.permute((0,2,3,1))
				p_masks = masks.permute((0,2,3,1))

				comb = torch.cat((p_out, p_edges, p_masks),1)
				for b in range(bs):
					im.imwrite(self.args.path + self.args.output_dir + "/edges_{}_E{}_{}.png".format(self.args.name, epoch, b), comb[b].cpu().detach().numpy())				

			#just env map model
			elif model == 2:
				if self.args.L2HDR:
					outputs, env_gen_loss, env_dis_loss, logs = self.env_model.process(images_ldr, env_maps, masks)
				else:
					outputs, env_gen_loss, env_dis_loss, logs = self.env_model.process(images, env_maps, masks)
				p_out = outputs.permute((0,2,3,1))
				p_env = env_maps.permute((0,2,3,1))
				p_masks = masks.permute((0,2,3,1)).repeat(1,1,1,3)

				comb = torch.cat((p_out, p_env),1)
				for b in range(bs):
					hdr = comb[b].cpu().detach().numpy()
					ones = np.ones_like(hdr)
					zeros = np.zeros_like(hdr)
					if self.args.log_norm == 1:
						save_img = np.exp(hdr+ones)-ones
						save_img = np.divide(save_img, log_alpha)
					elif self.args.log_norm == 2:
						hdr_pos = np.where(hdr < 0, 0, hdr)
						hdr_neg = np.negative(np.where(hdr > 0, 0, hdr))
						save_img = (np.exp(hdr_pos)-ones) - (np.exp(hdr_neg)-ones)
						save_img = np.divide(save_img, (0.2*hdr_means[b][1].item()))
					elif self.args.log_norm==3:
						save_img = gamma_correct(hdr,inverse=1)
					elif self.args.log_norm==4:
						save_img = np.divide(hdr,hdr_means[b][1].item()*0.5)
						save_img = gamma_correct(save_img,alpha=1,inverse=1)
					elif self.args.log_norm==5:
						hdr = hdr + ones
						save_img = gamma_correct(hdr,inverse=1)
					elif self.args.log_norm==6:
#						save_img = np.divide(hdr,hdr_means[b][1].item()*0.5)
						save_img = gamma_correct(hdr+ones,gamma=2.4,alpha=(0.5/hdr_means[b][1].item()),inverse=1)
					elif self.args.log_norm==7:
						save_img = gamma_correct(hdr+ones,gamma=self.args.gamma,inverse=1,alpha=1)
					elif self.args.log_norm == 8:
						save_img = np.exp(hdr)-ones
					elif self.args.log_norm == 9:
						ldr_norm = hdr[...,:3]
						hdr_log = hdr[...,3:]
						ones = np.ones_like(ldr_norm)
						ldr = np.divide(ldr_norm+ones,2)
						hdr_p = np.exp(hdr_log+ones)
						save_img = np.where(ldr>=1,hdr_p,ldr)
					else:
						save_img = hdr
					gen_img = data.tonemapping(save_img[:128]) #, self.args.path + self.args.output_dir + "/envs_{}_E{}_{}.png".format(self.args.name, epoch, b))
					gt_img = data.tonemapping(save_img[128:]) #, self.args.path + self.args.output_dir + "/envs_{}_E{}_{}.png".format(self.args.name, epoch, b))
					save_img = np.concatenate((gen_img, gt_img, p_masks[b].cpu().detach().numpy()),0)

					save_img = self.in_out(save_img)

#					save_img = np.concatenate((save_img[:128], save_img[128:], p_masks[b].cpu().detach().numpy()),0)
					im.imwrite(self.args.path + self.args.output_dir + "/envs_{}_E{}_{}.png".format(self.args.name, epoch, b), save_img)

#					data.tonemapping(save_img, self.args.path + self.args.output_dir + "/envs_{}_E{}_{}.png".format(self.args.name, epoch, b))
#					if self.args.log_norm:
#						save_img = torch.exp(comb[b])-1
#					else:
#						save_img =comb[b]
#					data.tonemapping(save_img.cpu().detach().numpy(), self.args.path + self.args.output_dir + "/envs_{}_E{}_{}.png".format(self.args.name, epoch, b))
#					data.tonemapping(comb[b].cpu().detach().numpy(), self.args.path + self.args.output_dir + "/envs_E{}_{}.png".format(epoch, b))
					#im.imwrite(self.args.path + self.args.output_dir + "/envs_{}_E{}_{}.exr".format(self.args.name, epoch, b), comb[b].cpu().detach().numpy())
				

			#just outpaint model 
			elif model == 3:
				# train
				if self.args.LDR:
					images_b = images_ldr
				else:
					images_b = images
				outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images_b, edges, masks, env_maps,images_ldr,epoch+1)
				outputs_merged = (outputs * masks) + (images * (1 - masks))

				p_out = outputs_merged.permute((0,2,3,1))
				p_images = images_b.permute((0,2,3,1))

				p_masks = masks.permute((0,2,3,1)).repeat(1,1,1,3)
				p_edges = edges.permute((0,2,3,1)).repeat(1,1,1,3)
				p_env_maps = env_maps.permute((0,2,3,1))

				comb = torch.cat((p_out, p_images, p_env_maps),1)
				for b in range(bs):
					hdr = comb[b].cpu().detach().numpy()
					ones = np.ones_like(hdr)
					if self.args.LDR == 0:
						if self.args.log_norm==1:
							save_img = np.exp(hdr+ones)-ones
						elif self.args.log_norm==6:
							hdr = hdr+ones
							save_img = hdr
							save_img[:128] = gamma_correct(hdr[:128],gamma=2.4,alpha=(0.5/hdr_means[b][0].item()),inverse=1)
							save_img[128:256] = gamma_correct(hdr[128:256],gamma=2.4,alpha=(0.5/hdr_means[b][0].item()),inverse=1)
							save_img[256:] = gamma_correct(hdr[256:],gamma=2.4,alpha=(0.5/hdr_means[b][1].item()),inverse=1)
						elif self.args.log_norm==7:
							save_img = gamma_correct(hdr+ones,gamma=self.args.gamma,inverse=1,alpha=1)
						gen_img = data.tonemapping(save_img[:128]) #, self.args.path + self.args.output_dir + "/envs_{}_E{}_{}.png".format(self.args.name, epoch, b))
						gt_img = data.tonemapping(save_img[128:256]) #, self.args.path + self.args.output_dir + "/envs_{}_E{}_{}.png".format(self.args.name, epoch, b))
						env_img = data.tonemapping(save_img[256:])
					else:
						save_img = np.divide(hdr+ones,2)
						gen_img = save_img[:128]
						gt_img = save_img[128:256]
						env_img = save_img[256:]
					save_img = np.concatenate((gen_img, gt_img, p_masks[b].cpu().detach().numpy(), env_img, p_edges[b].cpu().detach().numpy()),0)
					save_img = self.in_out(save_img)

					im.imwrite(self.args.path + self.args.output_dir + "/images_{}_E{}_{}.png".format(self.args.name,epoch, b), save_img)


			elif model == 6:
				if self.args.LDR:
					images_b = images_ldr
				else:
					images_b = images
				edge_outputs = self.edge_model.forward(ldr_img, edges, masks)
				env_outputs = self.env_model.forward(images_b, masks)
				outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images_b, edge_outputs, masks, env_outputs,images_ldr,epoch+1)
				outputs_merged = (outputs * masks) + (images * (1 - masks))

				p_out = outputs_merged.permute((0,2,3,1))
				p_images = images_b.permute((0,2,3,1))

				p_masks = masks.permute((0,2,3,1)).repeat(1,1,1,3)
				p_edges = edge_outputs.permute((0,2,3,1)).repeat(1,1,1,3).detach().cpu().numpy()	###!!!!
				p_edges_gt = edges.permute((0,2,3,1)).repeat(1,1,1,3).detach().cpu().numpy()	###!!!!
				p_env_maps = env_outputs.permute((0,2,3,1))
				p_env_maps_gt = env_maps.permute((0,2,3,1))

				noise = np.random.rand(self.args.height,self.args.width,3)

				comb = torch.cat((p_out, p_images, p_env_maps, p_env_maps_gt),1)
#				comb_gt = torch.cat((p_images, p_env_maps_gt),1)
#				comb = torch.cat((comb_pred, comb_gt),2)
				for b in range(bs):
					hdr = comb[b].cpu().detach().numpy()
					ones = np.ones_like(hdr)
					if self.args.LDR == 0:
						if self.args.log_norm==1:
							save_img = np.exp(hdr+ones)-ones
						elif self.args.log_norm==6:
							hdr = hdr+ones
							save_img = hdr
							save_img[:128] = gamma_correct(hdr[:128],gamma=2.4,alpha=(0.5/hdr_means[b][0].item()),inverse=1)
							save_img[128:256] = gamma_correct(hdr[128:256],gamma=2.4,alpha=(0.5/hdr_means[b][0].item()),inverse=1)
							save_img[256:] = gamma_correct(hdr[256:],gamma=2.4,alpha=(0.5/hdr_means[b][1].item()),inverse=1)
						elif self.args.log_norm==7:
							save_img = gamma_correct(hdr+ones,gamma=self.args.gamma,inverse=1,alpha=1)
#						print("DEBUG!! save_img shape: ", save_img.shape)
#						print("DEBUG!! gt shape: ", save_img[:128, 256:,:].shape)
#						print("DEBUG!! gen shape: ", save_img[:128,:256,:].shape)
#						pred = save_img[:128]
#						print("pred shape: ", pred.shape)
#						gt = save_img[128:]
#						print("gt shape: ", gt.shape)
						gen_img = data.tonemapping(save_img[:128]) #, self.args.path + self.args.output_dir + "/envs_{}_E{}_{}.png".format(self.args.name, epoch, b))
						gt_img = data.tonemapping(save_img[128:256]) #, self.args.path + self.args.output_dir + "/envs_{}_E{}_{}.png".format(self.args.name, epoch, b))
						env_img = data.tonemapping(save_img[256:384])
						gt_env_img = data.tonemapping(save_img[384:512])
					else:
						save_img = np.divide(hdr+ones,2)
						gen_img = save_img[:128]
						gt_img = save_img[128:256]
						env_img = save_img[256:]
					mask_img = p_masks[b].cpu().detach().numpy()
					pred_imgs = np.concatenate((gen_img, env_img,p_edges[b], gt_img*(1-mask_img)+mask_img*noise),0)
					gt_imgs = np.concatenate((gt_img, gt_env_img,p_edges_gt[b], mask_img),0)
					pred_imgs = self.in_out(pred_imgs)
					gt_imgs = self.in_out(gt_imgs)
					save_img = np.concatenate((pred_imgs, gt_imgs),1)
#					save_img = np.concatenate((gen_img, gt_img, p_masks[b].cpu().detach().numpy(), env_img, p_edges_gt[b].cpu().detach().numpy()),0)
#					save_img = self.in_out(save_img)

					im.imwrite(self.args.path + self.args.output_dir + "/images_ALL_E{}_{}.png".format(epoch, b), save_img)


#!!!DEBUG!!! removed log norm for LDR output
#					if self.args.log_norm == 1:
#						save_img = np.exp(hdr)-1
#					elif self.args.log_norm == 2:
#						hdr_pos = np.where(hdr < 0, 0, hdr)
#						hdr_neg = np.negative(np.where(hdr > 0, 0, hdr))
#						save_img = (np.exp(hdr_pos)-1) - (np.exp(hdr_neg)-1)
#					else:
#					save_img = hdr
#					data.tonemapping(save_img, self.args.path + self.args.output_dir + "/images_{}_E{}_{}.png".format(self.args.name, epoch, b))
#					if self.args.log_norm:
#						save_img = torch.exp(comb[b])-1
#					else:
#						save_img =comb[b]
#					data.tonemapping(save_img.cpu().detach().numpy(), self.args.path + self.args.output_dir + "/images_{}_E{}_{}.png".format(self.args.name, epoch, b))
#					data.tonemapping(comb[b].cpu().detach().numpy(), self.args.path + self.args.output_dir + "/images_E{}_{}.png".format(epoch, b))
#					im.imwrite(self.args.path + self.args.output_dir + "/images_{}_E{}_{}.exr".format(self.args.name,epoch, b), save_img)

	def print_testset(self, epoch):
		mode = self.args.mode
		bs = self.args.batch_size
		self.test_dataset.mask_step = 0
		#use test set
		loader = DataLoader(
			dataset=self.test_dataset,
			batch_size=bs,
			drop_last=True,
			shuffle=False
		)
		model = self.args.model
		log_alpha = self.args.log_alpha

		total = 0
		print("Printing test set")
		with torch.no_grad():
			for i, dataiter in enumerate(loader):
				images, images_gray, edges, masks, env_maps, ldr_img, hdr_means, hdr_true = self.cuda(dataiter,self.device)
				hdr_means = hdr_means.cpu().detach().numpy()
				#just edge model
				if model == 1:
					outputs, edge_gen_loss, edge_dis_loss, logs = self.edge_model.process(ldr_img, edges, masks)
					p_out = outputs.permute((0,2,3,1))

					for b in range(bs):
						im.imwrite(self.args.path + self.args.output_dir + "/edges_testset_out/edges_{}_{}.png".format(self.args.name, b+total), p_out[b].cpu().detach().numpy())

				#just env map model
				elif model == 2:
					outputs = self.env_model.forward(images, masks)
					p_out = outputs.permute((0,2,3,1))
					p_images = images.permute((0,2,3,1))

					comb = torch.cat((p_out, p_images),1)
					for b in range(bs):
						hdr = comb[b].cpu().detach().numpy()
						ones = np.ones_like(hdr)
						if self.args.log_norm == 1:
							save_img = np.exp(hdr+ones)-ones
							save_img = np.divide(save_img, log_alpha)
						elif self.args.log_norm == 2:
							hdr_pos = np.where(hdr < 0, 0, hdr)
							hdr_neg = np.negative(np.where(hdr > 0, 0, hdr))
							save_img = (np.exp(hdr_pos)-ones) - (np.exp(hdr_neg)-ones)
							save_img = np.divide(save_img, (0.2*hdr_means[b][1].item()))
						elif self.args.log_norm==3:
							save_img = gamma_correct(hdr,inverse=1)
						elif self.args.log_norm==4:
							save_img = np.divide(hdr,hdr_means[b][1].item()*0.5)
							save_img = gamma_correct(save_img,alpha=1,inverse=1)
						elif self.args.log_norm==5:
							hdr = hdr + ones
							save_img = gamma_correct(hdr,inverse=1)
						elif self.args.log_norm==6:
							hdr = hdr + ones
							save_img = np.divide(hdr,hdr_means[b][1].item()*0.5)
							save_img = gamma_correct(save_img,alpha=1,inverse=1)
						elif self.args.log_norm==7:
							hdr = hdr + ones
							save_img = gamma_correct(hdr,gamma=self.args.gamma,inverse=1,alpha=1)
						elif self.args.log_norm == 8:
							save_img = np.exp(hdr)-ones
						else:
							save_img = hdr
#						save_img = data.tonemapping(save_img)

						save_img = self.in_out(save_img)
						im.imwrite(self.args.path + self.args.output_dir + "/env_testsetEXR_out/envs_{}_fake_{}.exr".format(self.args.name, b+total), save_img[:128], 'EXR-FI')
						im.imwrite(self.args.path + self.args.output_dir + "/env_testsetEXR_data/envs_{}_{}.exr".format(self.args.name, b+total), save_img[128:], 'EXR-FI')

				#just outpaint model 
				elif model == 3:
					outputs, o_gen_loss, o_dis_loss, logs = self.outpaint_model.process(images, edges, masks, env_maps,ldr_img,10)
					outputs_merged = (outputs * masks) + (images * (1 - masks))

					p_out = outputs_merged.permute((0,2,3,1))
					for b in range(bs):
						hdr = p_out[b].cpu().detach().numpy()
						ones = np.ones_like(hdr)
						if self.args.log_norm==1:
							save_img = np.exp(hdr+ones)-ones
						elif self.args.log_norm==7:
							hdr = hdr + ones
							save_img = gamma_correct(hdr,gamma=self.args.gamma,inverse=1,alpha=1)
						if self.args.LDR == 0:
							gen_img = data.tonemapping(save_img)
						else:
							gen_img = save_img

						save_img = self.in_out(save_img)
						im.imwrite(self.args.path + self.args.output_dir + "/ICN_testset_out/images_{}_{}.png".format(self.args.name, b+total), save_img)

				total += bs*(i+1)



	def cuda(self, items,device):
		return (item.to(device) for item in items)
#		return (for item in items: item.to(device) if torch.is_tensor(item) else item)

	def batch_step(engine, batch):
		return batch

	def in_out(self, img):
		in_out_img = np.zeros_like(img)
		in_out_img[...,int(self.args.width/2):,:] = img[...,:int(self.args.width/2),:]
		in_out_img[...,:int(self.args.width/2),:] = img[...,int(self.args.width/2):,:]

		return in_out_img

	def log_unorm(self,ldr_hdr):
		ldr_norm = ldr_hdr[...,:3,:,:]
		hdr_log = ldr_hdr[...,3:,:,:]
		ones = torch.ones_like(ldr_norm).float()
		ldr = torch.div(ldr_norm+ones,2)
		hdr = torch.exp(hdr_log+ones)
		img_out = torch.where(ldr>=1,hdr,ldr)

		return img_out
