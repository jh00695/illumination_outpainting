import math
import helpers.spherical as SPH
import os, sys, gc

import torch
import torch.nn as nn
import torchvision.models as models
import torchvision.transforms as T

"""
    To Do:
    - think about other useful losses
"""

def parse_arguments(args):

    parser = argparse.ArgumentParser()
    parser.add_argument('--alpha', type=int, default=1)
    parser.add_argument('--e', type=int, default=1)
    parser.add_argument('--file', type=str, default="../dataset/EXR_images")
    parser.add_argument('--save_path', type=str, default="../outputs/cos_maps")
    parser.add_argument('--path', type=str, default="./")
    parser.add_argument('--width', type=int, default=512)
    parser.add_argument('--height', type=int, default=256)
    parser.add_argument('--colour', type=int, default=3)

    return parser.parse_known_args()

class AdversarialLoss(nn.Module):
    r"""
    Adversarial loss
    https://arxiv.org/abs/1711.10337
    """

    def __init__(self, type='nsgan', target_real_label=1.0, target_fake_label=0.0):
        r"""
        type = nsgan | lsgan | hinge
        """
        super(AdversarialLoss, self).__init__()

        self.type = type
        self.register_buffer('real_label', torch.tensor(target_real_label))
        self.register_buffer('fake_label', torch.tensor(target_fake_label))

        if type == 'nsgan':
            self.criterion = nn.BCELoss()

        elif type == 'lsgan':
            self.criterion = nn.MSELoss()

        elif type == 'hinge':
            self.criterion = nn.ReLU()

    def __call__(self, outputs, is_real, is_disc=None):
        if self.type == 'hinge':
            if is_disc:
                if is_real:
                    outputs = -outputs
                return self.criterion(1 + outputs).mean()
            else:
                return (outputs).mean()

        else:
            labels = (self.real_label if is_real else self.fake_label).expand_as(outputs)
            loss = self.criterion(outputs, labels)
            return loss


class StyleLoss(nn.Module):
    r"""
    Perceptual loss, VGG-based
    https://arxiv.org/abs/1603.08155
    https://github.com/dxyang/StyleTransfer/blob/master/utils.py
    """

    def __init__(self):
        super(StyleLoss, self).__init__()
        self.add_module('vgg', VGG19())
        self.criterion = torch.nn.L1Loss()

    def compute_gram(self, x):
        b, ch, h, w = x.size()
        f = x.view(b, ch, w * h)
        f_T = f.transpose(1, 2)
        G = f.bmm(f_T) / (h * w * ch)

        return G

    def __call__(self, x, y):
        # Compute features
        #x_vgg, y_vgg = self.vgg(x), self.vgg(y)
        if x.shape[1] == 6:
            ones = torch.ones_like(x[...,:3,:,:])
            x_vgg = self.vgg(torch.div(x[...,:3,:,:]+ones,2))
            y_vgg = self.vgg(torch.div(y[...,:3,:,:]+ones,2))
        else:
#            ones = torch.ones_like(x)
            x_vgg = self.vgg(x)
            y_vgg = self.vgg(y)

        # Compute loss
        style_loss = 0.0
        style_loss += self.criterion(self.compute_gram(x_vgg['relu2_2']), self.compute_gram(y_vgg['relu2_2']))
        style_loss += self.criterion(self.compute_gram(x_vgg['relu3_4']), self.compute_gram(y_vgg['relu3_4']))
        style_loss += self.criterion(self.compute_gram(x_vgg['relu4_4']), self.compute_gram(y_vgg['relu4_4']))
        style_loss += self.criterion(self.compute_gram(x_vgg['relu5_2']), self.compute_gram(y_vgg['relu5_2']))

        return style_loss



class PerceptualLoss(nn.Module):
    r"""
    Perceptual loss, VGG-based
    https://arxiv.org/abs/1603.08155
    https://github.com/dxyang/StyleTransfer/blob/master/utils.py
    """

    def __init__(self, weights=[1.0, 1.0, 1.0, 1.0, 1.0]):
        super(PerceptualLoss, self).__init__()
        self.add_module('vgg', VGG19())
        self.criterion = torch.nn.L1Loss()
        self.weights = weights

    def __call__(self, x, y):
        # Compute features
        #x_vgg, y_vgg = self.vgg(x), self.vgg(y)
        if x.shape[1] == 6:
            ones = torch.ones_like(x[...,:3,:,:])
            x_vgg = self.vgg(torch.div(x[...,:3,:,:]+ones,2))
            y_vgg = self.vgg(torch.div(y[...,:3,:,:]+ones,2))
        else:
#            ones = torch.ones_like(x)
            x_vgg = self.vgg(x)
            y_vgg = self.vgg(y)

        content_loss = 0.0
        content_loss += self.weights[0] * self.criterion(x_vgg['relu1_1'], y_vgg['relu1_1'])
        content_loss += self.weights[1] * self.criterion(x_vgg['relu2_1'], y_vgg['relu2_1'])
        content_loss += self.weights[2] * self.criterion(x_vgg['relu3_1'], y_vgg['relu3_1'])
        content_loss += self.weights[3] * self.criterion(x_vgg['relu4_1'], y_vgg['relu4_1'])
        content_loss += self.weights[4] * self.criterion(x_vgg['relu5_1'], y_vgg['relu5_1'])


        return content_loss



class VGG19(torch.nn.Module):
    def __init__(self):
        super(VGG19, self).__init__()
        features = models.vgg19(pretrained=True).features
        self.relu1_1 = torch.nn.Sequential()
        self.relu1_2 = torch.nn.Sequential()

        self.relu2_1 = torch.nn.Sequential()
        self.relu2_2 = torch.nn.Sequential()

        self.relu3_1 = torch.nn.Sequential()
        self.relu3_2 = torch.nn.Sequential()
        self.relu3_3 = torch.nn.Sequential()
        self.relu3_4 = torch.nn.Sequential()

        self.relu4_1 = torch.nn.Sequential()
        self.relu4_2 = torch.nn.Sequential()
        self.relu4_3 = torch.nn.Sequential()
        self.relu4_4 = torch.nn.Sequential()

        self.relu5_1 = torch.nn.Sequential()
        self.relu5_2 = torch.nn.Sequential()
        self.relu5_3 = torch.nn.Sequential()
        self.relu5_4 = torch.nn.Sequential()

        for x in range(2):
            self.relu1_1.add_module(str(x), features[x])

        for x in range(2, 4):
            self.relu1_2.add_module(str(x), features[x])

        for x in range(4, 7):
            self.relu2_1.add_module(str(x), features[x])

        for x in range(7, 9):
            self.relu2_2.add_module(str(x), features[x])

        for x in range(9, 12):
            self.relu3_1.add_module(str(x), features[x])

        for x in range(12, 14):
            self.relu3_2.add_module(str(x), features[x])

        for x in range(14, 16):
            self.relu3_3.add_module(str(x), features[x])

        for x in range(16, 18):
            self.relu3_4.add_module(str(x), features[x])

        for x in range(18, 21):
            self.relu4_1.add_module(str(x), features[x])

        for x in range(21, 23):
            self.relu4_2.add_module(str(x), features[x])

        for x in range(23, 25):
            self.relu4_3.add_module(str(x), features[x])

        for x in range(25, 27):
            self.relu4_4.add_module(str(x), features[x])

        for x in range(27, 30):
            self.relu5_1.add_module(str(x), features[x])

        for x in range(30, 32):
            self.relu5_2.add_module(str(x), features[x])

        for x in range(32, 34):
            self.relu5_3.add_module(str(x), features[x])

        for x in range(34, 36):
            self.relu5_4.add_module(str(x), features[x])

        # don't need the gradients, just want the features
        for param in self.parameters():
            param.requires_grad = False

    def forward(self, x):
        relu1_1 = self.relu1_1(x)
        relu1_2 = self.relu1_2(relu1_1)

        relu2_1 = self.relu2_1(relu1_2)
        relu2_2 = self.relu2_2(relu2_1)

        relu3_1 = self.relu3_1(relu2_2)
        relu3_2 = self.relu3_2(relu3_1)
        relu3_3 = self.relu3_3(relu3_2)
        relu3_4 = self.relu3_4(relu3_3)

        relu4_1 = self.relu4_1(relu3_4)
        relu4_2 = self.relu4_2(relu4_1)
        relu4_3 = self.relu4_3(relu4_2)
        relu4_4 = self.relu4_4(relu4_3)

        relu5_1 = self.relu5_1(relu4_4)
        relu5_2 = self.relu5_2(relu5_1)
        relu5_3 = self.relu5_3(relu5_2)
        relu5_4 = self.relu5_4(relu5_3)

        out = {
            'relu1_1': relu1_1,
            'relu1_2': relu1_2,

            'relu2_1': relu2_1,
            'relu2_2': relu2_2,

            'relu3_1': relu3_1,
            'relu3_2': relu3_2,
            'relu3_3': relu3_3,
            'relu3_4': relu3_4,

            'relu4_1': relu4_1,
            'relu4_2': relu4_2,
            'relu4_3': relu4_3,
            'relu4_4': relu4_4,

            'relu5_1': relu5_1,
            'relu5_2': relu5_2,
            'relu5_3': relu5_3,
            'relu5_4': relu5_4,
        }
        return out

"""
Coordinate system:
    cartesian on equirectangular pano:
    -   ranges from 0 to h/w
    -   starts at bottom left goes to top right

    lat lon on equirectangular pano:
    -   lat ranges from -pi to pi, starts at h to 0
    -   lon ranges from -pi to pi, starts at 0 goes to w

    polar coordinates:
    -   elevation starts at h, goes to 0
    -   azimuth starts at 0, goes to w
"""
class cosine_blur():
    def __init__(self, dims, device):
        self.batch_size = dims[0]
        self.colour = dims[1]
        self.height = dims[2]
        self.width  = dims[3]
        self.device = device
        self.half_height = int(math.floor(self.height/2))

        #generate attention weights and cosine weights
        spherical_weights = SPH.weights.spherical_confidence(SPH.grid.create_spherical_grid(self.width)).to(self.device).squeeze(0)
        self.attention_weights = spherical_weights[0][:self.half_height][:] #1,H,W
        phi_weights =  SPH.weights.theta_confidence(SPH.grid.create_spherical_grid(self.width)).to(self.device).squeeze(0)
        phi_hemisphere = phi_weights[0][:self.half_height][:]
        self.cosine_weights = torch.ones_like(phi_hemisphere) - phi_hemisphere

        # mapping equirect coordinate into LatLon coordinate system
        out_LonLat = torch_Pixel2LatLon(self.height,self.width,self.device)  # (H, W, (lat, lon))
        # mapping LatLon coordinate into xyz(sphere) coordinate system
        self.out_xyz = torch_LatLon2Sphere(out_LonLat)  # (H, W, (x, y, z))
        #convert lat lon to rotation matrix format
        out_LonLat[:,:,1] = out_LonLat[:,:,1] #longitude
        out_LonLat[:,:,0] = -1*(out_LonLat[:,:,0] - (math.pi/2))    #latitude
        #calculate all rotation matrices
        # make pair of xyz coordinate between src_xyz and out_xyz.
        # src_xyz @ R = out_xyz
        # src_xyz     = out_xyz @ R^t
        #rotation is gathered by using the negative of the lat lon coords
        self.Rt = torch_getAllRotMatrix(out_LonLat,self.device)
        #smoothing kernels
        self.pool = torch.nn.MaxPool2d(3,stride=1,padding=1)
        self.gaus = T.GaussianBlur(kernel_size=3,sigma=2)

        #del spherical_weights, phi_weights, phi_hemisphere, out_LonLat
        #gc.collect()


    def __call__(self,image, alpha, e):
        #image: (B, C, H, W)
        #output: (B, H, W, C)
        output = torch.zeros_like(image).permute((2,3,0,1)) #(H, W, B, C)
        hdr = image.flatten(2)
        #setup cosine power weights
        powCos = torch.pow(self.cosine_weights,alpha*e)
        #get index of first value lower than the minimum value cosine weights
        for y in range(powCos.shape[0]):
            if powCos[y][0].item() <= self.cosine_weights.min().item():
                 cutoff = y
                 break
        powCos_weights = powCos[...,:cutoff,:]
        for y in range(self.height):
            for x in range(self.width):
                #rotate spherical coordinates
#                src_xyz = torch.matmul(self.out_xyz, self.Rt[y][x])  # (H, W, (x, y, z))

                # mapping xyz(sphere) coordinate into LatLon into Pixel coordinate system
#               src_LonLat = torch_Sphere2LatLon(torch.matmul(self.out_xyz, self.Rt[y][x]))  # (H, W, 2)

                src_Pixel = torch_LatLon2Pixel(torch_Sphere2LatLon(torch.matmul(self.out_xyz, self.Rt[y][x])))  # (H, W, 2)
                h, w = src_Pixel[..., 0], src_Pixel[..., 1]
                z = h*self.width + w
                #src_Pixel_z = z.flatten(0).repeat(self.batch_size, self.colour,1)

                hemisphere_new = torch.gather(hdr, 2, z.flatten(0).repeat(self.batch_size, self.colour,1)).reshape_as(image)    # (B, C, H, W)
                hemisphere = hemisphere_new[...,:cutoff,:]

                #cosine_hemisphere = hemisphere * powCos_weights# * self.attention_weights
                output[y][x] = torch.mean(hemisphere * powCos_weights, (2,3)) # (B, C)

                #variables not reused: hemisphere, hemisphere_new, z, h, w, src_Pixel
                #del hemisphere, hemisphere_new, z, h, w, src_Pixel
                #gc.collect()    #test removing gc collect


        if alpha+e <= 5:
            output = self.gaus(output.permute((2,3,0,1)))
            return self.pool(output)#.permute((0,2,3,1)) # (B, H, W, C) maybe change to (B, C, H, W) when used as part of loss function
        else:
            return output.permute((2,3,0,1))
        


def torch_getAllRotMatrix(rotation, device):#rotation
    """
    :param rotation: (yaw, pitch, roll) in degree
    :return: general rotational matrix
    refer this: https://en.wikipedia.org/wiki/Rotation_matrix#General_rotations
    """
    #yaw, pitch, roll = (rotation / 180) * math.pi
    #input rotation shape:  [y, x, 2]
    #output shape: [y, x, 3, 3]
    Lat = rotation[:, :, 0].flatten()
    Lon = rotation[:, :, 1].flatten()

    zeros = torch.zeros_like(Lat)
    Rzyt = torch.stack([torch.stack([torch.cos(Lon)*torch.cos(Lat), torch.sin(Lon)*torch.cos(Lat), -torch.sin(Lat)]),
                        torch.stack([-torch.sin(Lon), torch.cos(Lon), zeros]),
                        torch.stack([torch.cos(Lon)*torch.sin(Lat), torch.sin(Lon)*torch.sin(Lat), torch.cos(Lat)])]).permute(2,0,1)

    output = Rzyt.reshape((rotation.shape[0],rotation.shape[1],3,3))
    return output

def torch_Pixel2LatLon(h, w, device):
  # LatLon (H, W, (lat, lon))
  Lat = (0.5 - torch.arange(0, h,dtype=torch.float,device=device)/h) * math.pi
  Lon = (torch.arange(0, w,dtype=torch.float,device=device)/w - 0.5) * 2 * math.pi

  Lat = torch.tile(Lat.unsqueeze(1), (w,))
  Lon = torch.tile(Lon, (h, 1))

  return torch.dstack((Lat, Lon))

def torch_LatLon2Sphere(LatLon):
  Lat = LatLon[:, :, 0]
  Lon = LatLon[:, :, 1]
  x = torch.cos(Lat) * torch.cos(Lon)
  y = torch.cos(Lat) * torch.sin(Lon)
  z = torch.sin(Lat)

  return torch.dstack((x, y, z))

def torch_Sphere2LatLon(xyz):
  Lat = math.pi / 2 - torch.arccos(xyz[:, :, 2])
  Lon = torch.atan2(xyz[:, :, 1], xyz[:, :, 0])

  return torch.dstack((Lat, Lon))

def torch_LatLon2Pixel(LatLon):
  h, w, _ = LatLon.shape
  Lat = LatLon[:, :, 0]
  Lon = LatLon[:, :, 1]
  i = (h * (0.5 - Lat / math.pi)) % h
  j = (w * (0.5 + Lon / (2 * math.pi))) % w

  return torch.dstack((i, j)).type(torch.int64)


                #rotate spherical coordinates
#                src_xyz = torch.matmul(self.out_xyz, self.Rt[y][x])  # (H, W, (x, y, z))

                # mapping xyz(sphere) coordinate into LatLon into Pixel coordinate system
#               src_LonLat = torch_Sphere2LatLon(src_xyz)  # (H, W, 2)
#
#                src_Pixel = torch_LatLon2Pixel(src_LonLat)  # (H, W, 2)
#                h, w = src_Pixel[..., 0], src_Pixel[..., 1]
#                z = h*self.width + w
#                src_Pixel_z = z.flatten(0).repeat(self.batch_size, self.colour,1)

#                hemisphere_new = torch.gather(hdr, 2, src_Pixel_z).reshape_as(image)    # (B, C, H, W)
#                hemisphere = hemisphere_new[...,:cutoff,:]

#                cosine_hemisphere = hemisphere * powCos_weights# * self.attention_weights
#                output[y][x] = torch.mean(cosine_hemisphere, (2,3)) # (B, C)

#variables not reused: cosine_hemisphere, hemisphere, hemisphere_new, src_Pixel_z, z, h, w, src_Pixel, src_xyz

if __name__ == '__main__':
    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
    print('Using device: ', device, ":", torch.cuda.current_device())
    #device = "cpu"

    args, unknown = parse_arguments(sys.argv)

    #check filename
    img_filename, img_extension = os.path.splitext(os.path.basename(args.file))
    #open file or file list
    if img_extension == '.exr':
        img = im.imread(args.file,'EXR-FI')
        img = cv2.resize(img, (args.width,args.height), interpolation=cv2.INTER_CUBIC)
        if args.colour == 1:
            img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        hdr_batch = torch.Tensor(img,device=device).permute((2,0,1)).unsqueeze(0)
        img_list = [args.file]

    elif img_extension == '.png' or img_extension == '.jpg':
        img = im.imread(args.file)
        img = cv2.resize(img, (args.width,args.height), interpolation=cv2.INTER_CUBIC)
        if args.colour == 1:
            img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        hdr_batch = torch.from_numpy(img).to(device).float().permute((2,0,1)).unsqueeze(0)
        img_list = [args.file]

    else:
        img_list = get_file_list(args.file)
        batch_size = 64

        blur_func = cosine_blur((batch_size,3,256,512), device)

        #declare batch [B,C,H,W]
        img_batch = torch.zeros((batch_size,3,256,512),dtype=torch.float,device=device)
        batch_list = []
        num_batches = 0
        for i, img_name in enumerate(img_list):
            batch_list.append(img_file)
            cur_index = i-(num_batches*batch_size)

            img_filename, img_extension = os.path.splitext(os.path.basename(img_name))

            if img_extension == ".exr":
                img = im.imread(img_name, 'EXR-FI')

            elif img_extension == ".jpg" or img_extension == ".png":
                img = im.imread(img_name)

            img = cv2.resize(img, (args.width,args.height), interpolation=cv2.INTER_CUBIC)
            if args.colour == 1:
                img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
            img_batch[cur_index] = torch.Tensor(img).to(device).permute((2,0,1))

            #process batch when it is full
            if cur_index == batch_size-1 or i+1 == len(img_list):
               num_batches +=1
               #loop for e values
               for e in range(1,101):
                   #get env_maps
                   env_maps = blur_func(img_batch, args.alpha, e).permute((0,2,3,1))
                   #save to dir
                   for b, img_name in enumerate(batch_list):
                       img_filename, img_extension = os.path.splitext(os.path.basename(img_name))
                       im.imwrite(args.path + args.save_path + img_filename + "_" + args.alpha + "_" + e + img_extension, env_maps[b].squeeze(0).cpu().detach().numpy())
                   batch_list = []
