import os
import numpy as np
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from .networks import OutpaintGenerator, EdgeGenerator, EnvGenerator, Discriminator, UNet
from .loss import AdversarialLoss, PerceptualLoss, StyleLoss, cosine_blur
from helpers.dataset_processing import gamma_correct_torch

import time


def gaussian(size,mean, std):
    noise = torch.randn(size, dtype=torch.float)*std+mean
    return noise

def uniform(size):
    return torch.rand(size, dtype=torch.float)*2-1

class BaseModel(nn.Module):
    def __init__(self, name, args):
        super(BaseModel, self).__init__()

        self.name = name
        self.args = args
        self.iteration = 0
        self.start_epoch = 0

        self.gen_weights_path = os.path.join(args.path + args.save_dir, name + '{}_gen.pth'.format(str(args.name)))
        self.dis_weights_path = os.path.join(args.path + args.save_dir, name + '{}_dis.pth'.format(str(args.name)))

        self.gen_val_path = os.path.join(args.path + args.save_dir + "/val_saves", name + '{}_gen.pth'.format(str(args.name)))
        self.dis_val_path = os.path.join(args.path + args.save_dir + "/val_saves", name + '{}_dis.pth'.format(str(args.name)))

        self.gen_load_path = os.path.join(args.path + args.load_dir, name + '_gen.pth')
        self.dis_load_path = os.path.join(args.path + args.load_dir, name + '_dis.pth')

        print("DEBUG!! load path: ", self.gen_load_path)

    def load(self):
        if os.path.exists(self.gen_load_path):
            print('Loading %s generator...' % self.name)

            if torch.cuda.is_available():
                data = torch.load(self.gen_load_path)
            else:
                data = torch.load(self.gen_load_path, map_location=lambda storage, loc: storage)

            self.generator.load_state_dict(data['generator'])
            self.iteration = data['iteration']
#            self.start_epoch = data['epoch']

        # load discriminator only when training
        if self.args.model == 1 and self.args.mode <= 2 and os.path.exists(self.dis_load_path):
            print('Loading %s discriminator...' % self.name)

            if torch.cuda.is_available():
                data = torch.load(self.dis_load_path)
            else:
                data = torch.load(self.dis_load_path, map_location=lambda storage, loc: storage)

            self.discriminator.load_state_dict(data['discriminator'])

    def checkpoint(self):
        if os.path.exists(self.gen_weights_path):
            print('Loading %s generator checkpoint...' % self.name)

            if torch.cuda.is_available():
                data = torch.load(self.gen_weights_path)
            else:
                data = torch.load(self.gen_weights_path, map_location=lambda storage, loc: storage)

            self.generator.load_state_dict(data['generator'])
            self.iteration = data['iteration']
            self.start_epoch = data['epoch']
            self.start_mask = data['mask']

            self.args.check = 1
        else:
            self.start_mask = -2
            self.args.check = 0

        # load discriminator only when training
        if self.args.mode <= 2 and os.path.exists(self.dis_weights_path):
            print('Loading %s discriminator checkpoint...' % self.name)

            if torch.cuda.is_available():
                data = torch.load(self.dis_weights_path)
            else:
                data = torch.load(self.dis_weights_path, map_location=lambda storage, loc: storage)

            self.discriminator.load_state_dict(data['discriminator'])

        #write loss values to csv
        if os.path.exists(self.gen_weights_path[:-4]+"_data.txt"):
            file = open(self.gen_weights_path[:-4]+"_data.txt","rb")
            gen_data = np.load(file, allow_pickle=True)
            file.close

            return gen_data

        else:
            return 'None'

    def save(self, epoch, mask, val=False,gen_data='None'):
        print('\nsaving %s...\n' % self.name)
        if val == True:
            gen_path = self.gen_val_path
            dis_path = self.dis_val_path
        else:
            gen_path = self.gen_weights_path
            dis_path = self.dis_weights_path

        torch.save({
            'iteration': self.iteration,
            'epoch': epoch,
            'generator': self.generator.state_dict(),
            'mask': mask
        }, gen_path)

        torch.save({
            'discriminator': self.discriminator.state_dict()
        }, dis_path)

        #write loss values to csv
        print("\nDEBGU!! type gen data: ",type(gen_data))
        if type(gen_data) != str:
            file = open(self.gen_weights_path[:-4]+"_data.txt","wb")
            np.save(file,gen_data)
            file.close

"""
    Inputs: grey images, GT edges, masks
    Outputs: edge_map
    Losses: l1 dsicriminator feature loss, adversarial loss(gen and dis)
"""
class EdgeModel(BaseModel):
    def __init__(self, args, device):
        super(EdgeModel, self).__init__('EdgeModel', args)

        # generator input: [grayscale(1) + edge(1) + mask(1)]
        # discriminator input: (grayscale(1) + edge(1))
#        generator = EdgeGenerator(use_spectral_norm=True,act_layer=args.act_layer)
        generator = UNet(n_classes=1,in_channels=3,sigmoid=1,bilinear='bilinear',act_layer='relu')
        discriminator = Discriminator(in_channels=2, use_sigmoid=True,act_layer='leaky')
        l1_loss = nn.L1Loss()
        adversarial_loss = AdversarialLoss(type=args.edge_loss)

        self.add_module('generator', generator)
        self.add_module('discriminator', discriminator)

        self.add_module('l1_loss', l1_loss)
        self.add_module('adversarial_loss', adversarial_loss)

        self.gen_optimizer = optim.Adam(
            params=generator.parameters(),
            lr=float(args.lr),
            betas=(args.beta1, args.beta2)
        )

        self.dis_optimizer = optim.Adam(
            params=discriminator.parameters(),
            lr=float(args.lr) * float(args.d2g_lr),
            betas=(args.beta1, args.beta2)
        )

    def process(self, images, edges, masks):
        self.iteration += 1

        # zero optimizers
        self.gen_optimizer.zero_grad()
        self.dis_optimizer.zero_grad()

        # process outputs
        outputs = self(images, edges, masks)
        gen_loss = 0
        dis_loss = 0

        # discriminator loss
        dis_input_real = torch.cat((images, edges), dim=1)
        dis_input_fake = torch.cat((images, outputs.detach()), dim=1)
        dis_real, dis_real_feat = self.discriminator(dis_input_real)        # in: (grayscale(1) + edge(1))
        dis_fake, dis_fake_feat = self.discriminator(dis_input_fake)        # in: (grayscale(1) + edge(1))
        dis_real_loss = self.adversarial_loss(dis_real, True, True)
        dis_fake_loss = self.adversarial_loss(dis_fake, False, True)
        dis_loss += (dis_real_loss + dis_fake_loss) / 2

        # generator adversarial loss
        gen_input_fake = torch.cat((images, outputs), dim=1)
        gen_fake, gen_fake_feat = self.discriminator(gen_input_fake)        # in: (grayscale(1) + edge(1))
        gen_gan_loss = self.adversarial_loss(gen_fake, True, False)
        gen_loss += gen_gan_loss

        # generator feature matching loss
        gen_fm_loss = 0
        for i in range(len(dis_real_feat)):
            gen_fm_loss += self.l1_loss(gen_fake_feat[i], dis_real_feat[i].detach())
        gen_fm_loss = gen_fm_loss * self.args.fm_lw
        gen_loss += gen_fm_loss

        #L1 loss for masked area
#        gen_l1_loss = self.l1_loss(outputs*masks,images*masks) * self.args.edg_l1_lw
#        gen_loss += gen_l1_loss

        # create logs
        logs = [
            ("l_d1", dis_loss.item()),
            ("l_g1", gen_gan_loss.item()),
            ("l_fm", gen_fm_loss.item()),
            ("l_el1", 0),
        ]

        return outputs, gen_loss, dis_loss, logs

    def forward(self, images, edges, masks):
        edges_masked = (edges * (1 - masks))
        images_masked = (images * (1 - masks)) + masks#*gaussian(images,0,0.05)
        inputs = torch.cat((images_masked, edges_masked, masks), dim=1)
        outputs = self.generator(inputs)                                    # in: [grayscale(1) + edge(1) + mask(1)]
        return outputs

    def backward(self, gen_loss=None, dis_loss=None):
        if dis_loss is not None:
            dis_loss.backward()
        if gen_loss is not None:
            gen_loss.backward()
        self.dis_optimizer.step()
        self.gen_optimizer.step()

"""
    Inputs: images, GT env maps, masks
    Outputs: environement map
    Losses: l2 environement map loss, l1 dsicriminator feature loss, adversarial loss(gen and dis)
"""
class EnvModel(BaseModel):
    def __init__(self, args,device):
        super(EnvModel, self).__init__('EnvModel', args)
        self.device = device
        self.noise_type = args.noise
        if args.log_norm == 9:
            channel = 6
        else:
            channel = 3
        self.size = (args.batch_size,channel,args.height,args.width)

        # generator input: [image(3) + mask(1)]
        # discriminator input: (env_map(3) + images(3))
        #generator = EnvGenerator(use_spectral_norm=True,act_layer=args.act_layer)

        generator = UNet(n_classes=channel,in_channels=channel+1,act_layer=args.act_layer,sigmoid=args.sigmoid,bilinear=args.upsample)
        discriminator = Discriminator(in_channels=channel*2, use_sigmoid=args.env_loss != 'hinge',act_layer=args.dsc_act_layer,init_weights=False)
        l1_loss = nn.L1Loss()
        adversarial_loss = AdversarialLoss(type=args.env_loss)
        l2_loss = nn.MSELoss()
        perceptual_loss = PerceptualLoss()
        style_loss = StyleLoss()


#        summary(generator.to(device).cuda(),(4,args.width,args.height))

        self.add_module('generator', generator)
        self.add_module('discriminator', discriminator)

        #add cos and l2 loss
        self.add_module('l1_loss', l1_loss)
        self.add_module('adversarial_loss', adversarial_loss)
        self.add_module('perceptual_loss', perceptual_loss)
        self.add_module('style_loss', style_loss)

        self.add_module('l2_loss', l2_loss)

        self.gen_optimizer = optim.Adam(
            params=generator.parameters(),
            lr=float(args.lr),
            betas=(args.beta1, args.beta2)
        )

        self.dis_optimizer = optim.Adam(
            params=discriminator.parameters(),
            lr=float(args.lr) * float(args.d2g_lr),
            betas=(args.beta1, args.beta2)
        )

    def process(self, images, env_maps, masks):
        self.iteration += 1


        # zero optimizers
        self.gen_optimizer.zero_grad()
        self.dis_optimizer.zero_grad()


        # process outputs
        outputs = self(images, masks)
        gen_loss = 0
        dis_loss = 0


        # discriminator loss
        dis_input_real = torch.cat((images, env_maps), dim=1)
        dis_input_fake = torch.cat((images, outputs.detach()), dim=1)
        dis_real, dis_real_feat = self.discriminator(dis_input_real)        # in: (grayscale(1) + edge(1))
        dis_fake, dis_fake_feat = self.discriminator(dis_input_fake)        # in: (grayscale(1) + edge(1))
        dis_real_loss = self.adversarial_loss(dis_real, True, True)
        dis_fake_loss = self.adversarial_loss(dis_fake, False, True)
        dis_loss += (dis_real_loss + dis_fake_loss) / 2


        # generator adversarial loss
        gen_input_fake = torch.cat((images, outputs), dim=1)
        gen_fake, gen_fake_feat = self.discriminator(gen_input_fake)        # in: (grayscale(1) + edge(1))
        gen_gan_loss = self.adversarial_loss(gen_fake, True, False)
        gen_loss += gen_gan_loss


        # generator feature matching loss
#        gen_fm_loss = 0
#        for i in range(len(dis_real_feat)):
#            gen_fm_loss += self.l1_loss(gen_fake_feat[i], dis_real_feat[i].detach())
#        gen_fm_loss = gen_fm_loss * self.args.fm_lw
#        gen_loss += gen_fm_loss

        #L1 env map loss
        gen_l1_loss = self.l1_loss(outputs[...,:3,:,:],env_maps[...,:3,:,:]) * self.args.env_l1_lw
#        gen_l1_loss = torch.zeros(1).float()
        gen_loss +=  gen_l1_loss

        if self.args.log_norm == 9:
            gen_l1_hdr_loss = self.l1_loss(outputs[...,3:,:,:],env_maps[...,3:,:,:])*self.args.env_l1_hdr_lw
            gen_loss += gen_l1_hdr_loss
        else:
            gen_l1_hdr_loss = torch.zeros(1).float()

        # generator perceptual loss
        gen_content_loss = self.perceptual_loss(outputs, env_maps)
        gen_content_loss = gen_content_loss * self.args.content_lw
        gen_loss += gen_content_loss

        # generator style loss
        gen_style_loss = self.style_loss(outputs, env_maps)
        gen_style_loss = gen_style_loss * self.args.style_lw
        gen_loss += gen_style_loss

        #hdr loss add to loss function later if needed
#        gen_hdr_loss = self.l1_loss(gamma_correct_torch(outputs),env_maps) * self.args.env_hdr_lw
        #gen_loss += gen_hdr_loss
        
        # create logs
        logs = [
            ("l_d1", dis_loss.item()),
            ("l_g1", gen_gan_loss.item()),
#            ("l_fm", gen_fm_loss.item()),
            ("l_per", gen_content_loss.item()),
            ("l_sty", gen_style_loss.item()),
            ("l_l1", gen_l1_loss.item()),
            ("l_l1_hdr", gen_l1_hdr_loss.item()),

#            ("l_hdr", gen_hdr_loss.item()),
        ]

        return outputs, gen_loss, dis_loss, logs

    def forward(self, images, masks):
        if self.noise_type == 1:
            noise = gaussian(self.size,0.5,0.5).to(self.device)
        elif self.noise_type == 2:
            noise = uniform(self.size).to(self.device)
        else:
            noise = 0
#        print("DEBUG!! noise shape: ", noise.shape)
#        print("DEBUG!! masks shape: ", masks.shape)
#        print("DEBGU!! images shape: ", images.shape)
        #edges_masked = (env_maps * (1 - masks))
        images_masked = (images * (1 - masks)) + masks * noise
        inputs = torch.cat((images_masked, masks), dim=1)
        outputs = self.generator(inputs)                                    # in: [grayscale(1) + edge(1) + mask(1)]
        return outputs

    def backward(self, gen_loss=None, dis_loss=None):
        if dis_loss is not None:
            dis_loss.backward()
        if gen_loss is not None:
            gen_loss.backward()

        self.dis_optimizer.step()
        self.gen_optimizer.step()

"""
    Inputs: images, GT edges, GT, env maps, masks
    Outputs: generated HDR
    Losses: l2 cosine loss, l1 loss, style loss, perceptual loss,adversarial loss(gen and dis)
"""
class OutpaintingModel(BaseModel):
    def __init__(self, args, device):
        super(OutpaintingModel, self).__init__('OutpaintingModel', args)

        self.device = device
        self.noise_type = args.noise
        self.size = (args.batch_size,3,args.height,args.width)
        # generator input: [rgb(3) + edge(1)]
        # discriminator input: [rgb(3)]
        #generator = OutpaintGenerator(act_layer=args.act_layer)
        if self.args.singleOUT == 0:
            chana = 8
        elif self.args.singleOUT == 1:
            chana = 4
        elif self.args.singleOUT == 2:
            chana = 5
        elif self.args.singleOUT == 3:
            chana = 7
        if args.network == 0:
            generator = UNet(n_classes=3,in_channels=chana,act_layer=args.act_layer,sigmoid=args.sigmoid,bilinear=args.upsample)
        else:
            generator = OutpaintGenerator(in_channels=chana, act_layer=args.act_layer)
        discriminator = Discriminator(in_channels=3, use_sigmoid=args.out_loss != 'hinge',act_layer=args.dsc_act_layer)

        self.alpha = args.alpha

        l1_loss = nn.L1Loss()
        perceptual_loss = PerceptualLoss()
        style_loss = StyleLoss()
        adversarial_loss = AdversarialLoss(type=args.out_loss)
        self.cosine_blur = cosine_blur((args.batch_size*2,args.colour,int(args.res/2),args.res),device)
        l2_loss = nn.MSELoss()

        self.noise_type = args.noise

        self.add_module('generator', generator)
        self.add_module('discriminator', discriminator)

        self.add_module('l1_loss', l1_loss)
        self.add_module('perceptual_loss', perceptual_loss)
        self.add_module('style_loss', style_loss)
        self.add_module('adversarial_loss', adversarial_loss)
        #self.add_module('cosine_blur', cosine)
        self.add_module('l2_loss', l2_loss)

        self.gen_optimizer = optim.Adam(
            params=generator.parameters(),
            lr=float(args.lr),
            betas=(args.beta1, args.beta2)
        )

        self.dis_optimizer = optim.Adam(
            params=discriminator.parameters(),
            lr=float(args.lr) * float(args.d2g_lr),
            betas=(args.beta1, args.beta2)
        )

    def process(self, images, edges, masks, env_maps,ldr_img,e):
        self.iteration += 1

        # zero optimizers
        self.gen_optimizer.zero_grad()
        self.dis_optimizer.zero_grad()

        if self.args.LDR:
            images = ldr_img

        # process outputs
        if self.args.L2HDR:
            outputs = self(ldr_img, edges, masks, env_maps) #L2HDR ldr_img instead of images
        else:
            outputs = self(images, edges, masks, env_maps) #L2HDR ldr_img instead of images
        gen_loss = 0
        dis_loss = 0

        # discriminator loss
        dis_input_real = images
        dis_input_fake = outputs.detach()
        dis_real, _ = self.discriminator(dis_input_real)                    # in: [rgb(3)]
        dis_fake, _ = self.discriminator(dis_input_fake)                    # in: [rgb(3)]
        dis_real_loss = self.adversarial_loss(dis_real, True, True)
        dis_fake_loss = self.adversarial_loss(dis_fake, False, True)
        dis_loss += (dis_real_loss + dis_fake_loss) / 2

        # generator adversarial lo
        gen_input_fake = outputs
        gen_fake, _ = self.discriminator(gen_input_fake)                    # in: [rgb(3)]
        gen_gan_loss = self.adversarial_loss(gen_fake, True, False) * self.args.outpaint_adv_lw
        gen_loss += gen_gan_loss

        # generator l1 loss
        gen_l1_loss = self.l1_loss(outputs, images) * self.args.l1_lw / torch.mean(masks)
        gen_loss += gen_l1_loss

        # generator unknown l1 loss
#        gen_Ul1_loss = self.l1_loss(outputs*masks, images*masks) * self.args.l1_lw / torch.mean(masks)
#        gen_loss += gen_Ul1_loss


        # generator perceptual lo
        gen_content_loss = self.perceptual_loss(outputs, images)
        gen_content_loss = gen_content_loss * self.args.content_lw
        gen_loss += gen_content_loss

        # generator style loss
        gen_style_loss = self.style_loss(outputs * masks, images * masks)
        gen_style_loss = gen_style_loss * self.args.style_lw
        gen_loss += gen_style_loss

        #generator cosine loss
        """
        out_cos = self.cosine_blur(outputs.detach(),self.alpha,e)
        in_cos = self.cosine_blur(images.detach(),self.alpha,e)
        gen_cos_loss = self.l2_loss(out_cos, in_cos)*self.args.cos_lw
        gen_loss += gen_cos_loss
        """
        if self.args.cos_lw > 0:
            blur_batch = torch.zeros((images.shape[0]+outputs.shape[0],images.shape[1],images.shape[2],images.shape[3]),dtype=torch.float,device=self.device)
            blur_batch[:outputs.shape[0]] = outputs.detach()
            blur_batch[outputs.shape[0]:] = images.detach()
            blur_batch = F.interpolate(blur_batch, (int(self.args.res/2),self.args.res), mode='bicubic')
            batch_cos = self.cosine_blur(blur_batch,self.alpha,1)
            gen_cos_loss = self.l2_loss(batch_cos[:outputs.shape[0]], batch_cos[outputs.shape[0]:])*self.args.cos_lw
            gen_loss += gen_cos_loss
        else:
            gen_cos_loss = torch.zeros(1).float()

        # create logs
        logs = [
            ("l_d2", dis_loss.item()),
            ("l_g2", gen_gan_loss.item()),
            ("l_l1", gen_l1_loss.item()),
            ("l_per", gen_content_loss.item()),
            ("l_sty", gen_style_loss.item()),
            ("l_cos", gen_cos_loss.item()),
 #           ("l_Ul1", gen_Ul1_loss.item()),
        ]

        return outputs.detach(), gen_loss, dis_loss, logs

    def forward(self, images, edges, masks, env_maps):
        if self.noise_type == 1:
            noise = gaussian(self.size,0.5,0.5).to(self.device)
        elif self.noise_type == 2:
            noise = uniform(self.size).to(self.device)
        else:
            noise = 0
        images_masked = (images * (1 - masks).float()) + masks * noise
        if self.args.singleOUT == 0:
            inputs = torch.cat((images_masked, edges, env_maps, masks), dim=1)
        elif self.args.singleOUT == 1:
            inputs = torch.cat((images_masked, masks), dim=1)
        elif self.args.singleOUT == 2:
            inputs = torch.cat((images_masked, edges, masks), dim=1)
        elif self.args.singleOUT == 3:
            inputs = torch.cat((images_masked, env_maps, masks), dim=1)
        
        outputs = self.generator(inputs)                                    # in: [rgb(3) + edge(1)]
        return outputs

    def backward(self, gen_loss=None, dis_loss=None):
        dis_loss.backward()
        gen_loss.backward()

        self.dis_optimizer.step()
        self.gen_optimizer.step()

"""
    Inputs: images, GT, masks
    Outputs: generated HDR
    Losses: l2 cosine loss, l1 loss, style loss, perceptual loss,adversarial loss(gen and dis)
"""
class singleOutpaintingModel(BaseModel):
    def __init__(self, args, device):
        super(OutpaintingModel, self).__init__('OutpaintingModel', args)

        self.device = device

        # generator input: [rgb(3) + edge(1)]
        # discriminator input: [rgb(3)]
        #generator = OutpaintGenerator(act_layer=args.act_layer)
        generator = UNet(n_classes=3,in_channels=4,act_layer=args.act_layer,sigmoid=args.sigmoid)
        discriminator = Discriminator(in_channels=3, use_sigmoid=args.out_loss != 'hinge',act_layer=args.dsc_act_layer)

        self.alpha = args.alpha

        l1_loss = nn.L1Loss()
        perceptual_loss = PerceptualLoss()
        style_loss = StyleLoss()
        adversarial_loss = AdversarialLoss(type=args.out_loss)
        self.cosine_blur = cosine_blur((args.batch_size*2,args.colour,int(args.res/2),args.res),device)
        l2_loss = nn.MSELoss()

        self.noise_type = args.noise

        self.add_module('generator', generator)
        self.add_module('discriminator', discriminator)

        self.add_module('l1_loss', l1_loss)
        self.add_module('perceptual_loss', perceptual_loss)
        self.add_module('style_loss', style_loss)
        self.add_module('adversarial_loss', adversarial_loss)
        #self.add_module('cosine_blur', cosine)
        self.add_module('l2_loss', l2_loss)

        self.gen_optimizer = optim.Adam(
            params=generator.parameters(),
            lr=float(args.lr),
            betas=(args.beta1, args.beta2)
        )

        self.dis_optimizer = optim.Adam(
            params=discriminator.parameters(),
            lr=float(args.lr) * float(args.d2g_lr),
            betas=(args.beta1, args.beta2)
        )

    def process(self, images, edges, masks, env_maps,e):
        self.iteration += 1

        # zero optimizers
        self.gen_optimizer.zero_grad()
        self.dis_optimizer.zero_grad()

        # process outputs
        outputs = self(images, edges, masks, env_maps)
        gen_loss = 0
        dis_loss = 0

        # discriminator loss
        dis_input_real = images
        dis_input_fake = outputs.detach()
        dis_real, _ = self.discriminator(dis_input_real)                    # in: [rgb(3)]
        dis_fake, _ = self.discriminator(dis_input_fake)                    # in: [rgb(3)]
        dis_real_loss = self.adversarial_loss(dis_real, True, True)
        dis_fake_loss = self.adversarial_loss(dis_fake, False, True)
        dis_loss += (dis_real_loss + dis_fake_loss) / 2

        # generator adversarial lo
        gen_input_fake = outputs
        gen_fake, _ = self.discriminator(gen_input_fake)                    # in: [rgb(3)]
        gen_gan_loss = self.adversarial_loss(gen_fake, True, False) * self.args.outpaint_adv_lw
        gen_loss += gen_gan_loss

        # generator l1 loss
        gen_l1_loss = self.l1_loss(outputs, images) * self.args.l1_lw / torch.mean(masks)
        gen_loss += gen_l1_loss

        # generator unknown l1 loss
        gen_Ul1_loss = self.l1_loss(outputs*masks, images*masks) * self.args.l1_lw / torch.mean(masks)
        gen_loss += gen_Ul1_loss


        # generator perceptual lo
        gen_content_loss = self.perceptual_loss(outputs, images)
        gen_content_loss = gen_content_loss * self.args.content_lw
        gen_loss += gen_content_loss

        # generator style loss
        gen_style_loss = self.style_loss(outputs * masks, images * masks)
        gen_style_loss = gen_style_loss * self.args.style_lw
        gen_loss += gen_style_loss

        #generator cosine loss
        """
        out_cos = self.cosine_blur(outputs.detach(),self.alpha,e)
        in_cos = self.cosine_blur(images.detach(),self.alpha,e)
        gen_cos_loss = self.l2_loss(out_cos, in_cos)*self.args.cos_lw
        gen_loss += gen_cos_loss
        """
        blur_batch = torch.zeros((images.shape[0]+outputs.shape[0],images.shape[1],images.shape[2],images.shape[3]),dtype=torch.float,device=self.device)
        blur_batch[:outputs.shape[0]] = outputs.detach()
        blur_batch[outputs.shape[0]:] = images.detach()
        blur_batch = F.interpolate(blur_batch, (int(self.args.res/2),self.args.res), mode='bicubic')
        batch_cos = self.cosine_blur(blur_batch,self.alpha,e)
        gen_cos_loss = self.l2_loss(batch_cos[:outputs.shape[0]], batch_cos[outputs.shape[0]:])*self.args.cos_lw
        gen_loss += gen_cos_loss

        # create logs
        logs = [
            ("l_d2", dis_loss.item()),
            ("l_g2", gen_gan_loss.item()),
            ("l_l1", gen_l1_loss.item()),
            ("l_per", gen_content_loss.item()),
            ("l_sty", gen_style_loss.item()),
            ("l_cos", gen_cos_loss.item()),
            ("l_Ul1", gen_Ul1_loss.item()),
        ]

        return outputs, gen_loss, dis_loss, logs

    def forward(self, images, edges, masks, env_maps):
        if self.noise_type == 1:
            noise = gaussian((self.args.batch_size,3,self.args.height,self.args.width),0,0.05).to(self.device)
        elif self.noise_type == 2:
            noise = uniform((self.args.batch_size,3,self.args.height,self.args.width)).to(self.device)
        else:
            noise = 0
        images_masked = (images * (1 - masks).float()) + masks * noise
        inputs = torch.cat((images_masked, masks), dim=1) #edges, env_maps), dim=1)
        outputs = self.generator(inputs)                                    # in: [rgb(3) + edge(1)]
        return outputs

    def backward(self, gen_loss=None, dis_loss=None):
        dis_loss.backward()
        gen_loss.backward()

        self.dis_optimizer.step()
        self.gen_optimizer.step()
