import torch
import torch.nn as nn
import torch.nn.functional as F

"""
    To Do:
    - test relu and elu for hdr inputs
    - add option to switch between each in disrciminator
"""
class BaseNetwork(nn.Module):
    def __init__(self):
        super(BaseNetwork, self).__init__()

    def init_weights(self, init_type='normal', gain=0.02):
        '''
        initialize network's weights
        init_type: normal | xavier | kaiming | orthogonal
        https://github.com/junyanz/pytorch-CycleGAN-and-pix2pix/blob/9451e70673400885567d08a9e97ade2524c700d0/models/networks.py#L39
        '''

        def init_func(m):
            classname = m.__class__.__name__
            if hasattr(m, 'weight') and (classname.find('Conv') != -1 or classname.find('Linear') != -1):
                if init_type == 'normal':
                    nn.init.normal_(m.weight.data, 0.0, gain)
                elif init_type == 'xavier':
                    nn.init.xavier_normal_(m.weight.data, gain=gain)
                elif init_type == 'kaiming':
                    nn.init.kaiming_normal_(m.weight.data, a=0, mode='fan_in')
                elif init_type == 'orthogonal':
                    nn.init.orthogonal_(m.weight.data, gain=gain)

                if hasattr(m, 'bias') and m.bias is not None:
                    nn.init.constant_(m.bias.data, 0.0)

            elif classname.find('BatchNorm2d') != -1:
                nn.init.normal_(m.weight.data, 1.0, gain)
                nn.init.constant_(m.bias.data, 0.0)

        self.apply(init_func)


#increase in channels to 7 for added env_map
class OutpaintGenerator(BaseNetwork):
    def __init__(self, in_channels=7, residual_blocks=8, init_weights=True, act_layer='elu'):
        super(OutpaintGenerator, self).__init__()

        enc = []
        enc.append(nn.Sequential(nn.ReflectionPad2d(3),
            nn.Conv2d(in_channels=in_channels, out_channels=64, kernel_size=7, padding=0),
            nn.InstanceNorm2d(64, track_running_stats=False)))
        if act_layer == 'elu':
            enc.append(nn.ELU(True))
        elif act_layer == 'leaky':
            enc.append(nn.LeakyReLU(inplace=True))
        else:
            enc.append(nn.ReLU(True))
        enc.append(nn.Sequential(nn.Conv2d(in_channels=64, out_channels=128, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(128, track_running_stats=False)))
        if act_layer == 'elu':
            enc.append(nn.ELU(True))
        elif act_layer == 'leaky':
            enc.append(nn.LeakyReLU(inplace=True))
        else:
            enc.append(nn.ReLU(True))
        enc.append(nn.Sequential(nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(256, track_running_stats=False)))
        if act_layer == 'elu':
            enc.append(nn.ELU(True))
        elif act_layer == 'leaky':
            enc.append(nn.LeakyReLU(inplace=True))
        else:
            enc.append(nn.ReLU(True))
        self.encoder = nn.Sequential(*enc)
        

        blocks = []
        for _ in range(residual_blocks):
            block = ResnetBlock(256, 2)
            blocks.append(block)

        self.middle = nn.Sequential(*blocks)

        
        dec = []
        dec.append(nn.Sequential(nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(128, track_running_stats=False)))
        if act_layer == 'elu':
            dec.append(nn.ELU(True))
        elif act_layer == 'leaky':
            enc.append(nn.LeakyReLU(inplace=True))
        else:
            dec.append(nn.ReLU(True))

        dec.append(nn.Sequential(nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(64, track_running_stats=False)))
        if act_layer == 'elu':
            dec.append(nn.ELU(True))
        elif act_layer == 'leaky':
            enc.append(nn.LeakyReLU(inplace=True))
        else:
            dec.append(nn.ReLU(True))

        dec.append(nn.Sequential(nn.ReflectionPad2d(3),
            nn.Conv2d(in_channels=64, out_channels=3, kernel_size=7, padding=0)))
        self.decoder = nn.Sequential(*dec)

        if init_weights:
            self.init_weights()

    def forward(self, x):
        x = self.encoder(x)
        x = self.middle(x)
        x = self.decoder(x)
        #x = (torch.tanh(x) + 1) / 2
        x = torch.tanh(x)
        return x

#may change elu in decoder back to relu
class EdgeGenerator(BaseNetwork):
    def __init__(self, residual_blocks=8, use_spectral_norm=True, init_weights=True,act_layer='elu'):
        super(EdgeGenerator, self).__init__()

        enc = []
        enc.append(nn.Sequential(nn.ReflectionPad2d(3),
            nn.Conv2d(in_channels=3, out_channels=64, kernel_size=7, padding=0),
            nn.InstanceNorm2d(64, track_running_stats=False)))
        if act_layer == 'elu':
            enc.append(nn.ELU(True))
        else:
            enc.append(nn.ReLU(True))
        enc.append(nn.Sequential(nn.Conv2d(in_channels=64, out_channels=128, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(128, track_running_stats=False)))
        if act_layer == 'elu':
            enc.append(nn.ELU(True))
        else:
            enc.append(nn.ReLU(True))
        enc.append(nn.Sequential(nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(256, track_running_stats=False)))
        if act_layer == 'elu':
            enc.append(nn.ELU(True))
        else:
            enc.append(nn.ReLU(True))
        self.encoder = nn.Sequential(*enc)

        blocks = []
        for _ in range(residual_blocks):
            block = ResnetBlock(256, 2, use_spectral_norm=use_spectral_norm, act_layer=act_layer)
            blocks.append(block)

        self.middle = nn.Sequential(*blocks)

        dec = []
        dec.append(nn.Sequential(nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(128, track_running_stats=False)))
        if act_layer == 'elu':
            dec.append(nn.ELU(True))
        else:
            dec.append(nn.ReLU(True))

        dec.append(nn.Sequential(nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(64, track_running_stats=False)))
        if act_layer == 'elu':
            dec.append(nn.ELU(True))
        else:
            dec.append(nn.ReLU(True))

        dec.append(nn.Sequential(nn.ReflectionPad2d(3),
            nn.Conv2d(in_channels=64, out_channels=1, kernel_size=7, padding=0)))
        self.decoder = nn.Sequential(*dec)

        if init_weights:
            self.init_weights()

    def forward(self, x):
        x = self.encoder(x)
        x = self.middle(x)
        x = self.decoder(x)
        x = torch.sigmoid(x)
        return x

#incrreased in channels to 4 for mask and image
class EnvGenerator(BaseNetwork):
    def __init__(self, residual_blocks=8, use_spectral_norm=False, init_weights=False, act_layer='elu'):
        super(EnvGenerator, self).__init__()

        enc = []
        enc.append(nn.Sequential(nn.ReflectionPad2d(3),
            nn.Conv2d(in_channels=4, out_channels=64, kernel_size=7, padding=0),
            nn.InstanceNorm2d(64, track_running_stats=False)))
        if act_layer == 'elu':
            enc.append(nn.ELU(True))
        else:
            enc.append(nn.ReLU(True))
        enc.append(nn.Sequential(nn.Conv2d(in_channels=64, out_channels=128, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(128, track_running_stats=False)))
        if act_layer == 'elu':
            enc.append(nn.ELU(True))
        else:
            enc.append(nn.ReLU(True))
        enc.append(nn.Sequential(nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(256, track_running_stats=False)))
        if act_layer == 'elu':
            enc.append(nn.ELU(True))
        else:
            enc.append(nn.ReLU(True))
        self.encoder = nn.Sequential(*enc)

        blocks = []
        for _ in range(residual_blocks):
            block = ResnetBlock(256, 2, use_spectral_norm=use_spectral_norm)
            blocks.append(block)

        self.middle = nn.Sequential(*blocks)

        dec = []
        dec.append(nn.Sequential(nn.ConvTranspose2d(in_channels=256, out_channels=128, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(128, track_running_stats=False)))
        if act_layer == 'elu':
            dec.append(nn.ELU(True))
        else:
            dec.append(nn.ReLU(True))

        dec.append(nn.Sequential(nn.ConvTranspose2d(in_channels=128, out_channels=64, kernel_size=4, stride=2, padding=1),
            nn.InstanceNorm2d(64, track_running_stats=False)))
        if act_layer == 'elu':
            dec.append(nn.ELU(True))
        else:
            dec.append(nn.ReLU(True))

        dec.append(nn.Sequential(nn.ReflectionPad2d(3),
            nn.Conv2d(in_channels=64, out_channels=3, kernel_size=7, padding=0)))
        self.decoder = nn.Sequential(*dec)

        if init_weights:
            self.init_weights()

    def forward(self, x):
        x = self.encoder(x)
        x = self.middle(x)
        x = self.decoder(x)
        #x = torch.sigmoid(x)
        #x = (torch.tanh(x) + 1) / 2
        x = torch.tanh(x)
        return x


class Discriminator(BaseNetwork):
    def __init__(self, in_channels, use_sigmoid=True, use_spectral_norm=True, init_weights=True,act_layer=None):
        super(Discriminator, self).__init__()
        self.use_sigmoid = use_sigmoid

        if act_layer == 'elu':
            self.activation = nn.ELU(True)
        elif act_layer == 'relu':
            self.activation = nn.ReLU(True)
        else:
            self.activation = nn.LeakyReLU(0.2, inplace=True)

        self.conv1 = self.features = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels=in_channels, out_channels=64, kernel_size=4, stride=2, padding=1, bias=not use_spectral_norm), use_spectral_norm),
            #nn.LeakyReLU(0.2, inplace=True),
        )

        self.conv2 = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels=64, out_channels=128, kernel_size=4, stride=2, padding=1, bias=not use_spectral_norm), use_spectral_norm),
            #nn.LeakyReLU(0.2, inplace=True),
        )

        self.conv3 = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels=128, out_channels=256, kernel_size=4, stride=2, padding=1, bias=not use_spectral_norm), use_spectral_norm),
#            nn.LeakyReLU(0.2, inplace=True),
        )

        self.conv4 = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels=256, out_channels=512, kernel_size=4, stride=1, padding=1, bias=not use_spectral_norm), use_spectral_norm),
#            nn.LeakyReLU(0.2, inplace=True),
        )

        self.conv5 = nn.Sequential(
            spectral_norm(nn.Conv2d(in_channels=512, out_channels=1, kernel_size=4, stride=1, padding=1, bias=not use_spectral_norm), use_spectral_norm),
        )

        if init_weights:
            self.init_weights()

        if self.use_sigmoid != True:
            self.FC = nn.Linear(1024*8*4,1)

    def forward(self, x):
        conv1 = self.activation(self.conv1(x))
        conv2 = self.activation(self.conv2(conv1))
        conv3 = self.activation(self.conv3(conv2))
        conv4 = self.activation(self.conv4(conv3))
        conv5 = self.activation(self.conv5(conv4))

        outputs = conv5
        if self.use_sigmoid:
            outputs = torch.sigmoid(conv5)
        else:
            flat = conv5.view(-1, 1024*8*4)
            outputs = self.FC(flat)

        return outputs, [conv1, conv2, conv3, conv4, conv5]


class ResnetBlock(nn.Module):
    def __init__(self, dim, dilation=1, use_spectral_norm=False, act_layer='elu'):
        super(ResnetBlock, self).__init__()
        
        res = []
        res.append(nn.Sequential(nn.ReflectionPad2d(dilation),
            spectral_norm(nn.Conv2d(in_channels=dim, out_channels=dim, kernel_size=3, padding=0, dilation=dilation, bias=not use_spectral_norm), use_spectral_norm),
            nn.InstanceNorm2d(dim, track_running_stats=False)))
        if act_layer == 'elu':
            res.append(nn.ELU(True))
        else:
            res.append(nn.ReLU(True))

        res.append(nn.Sequential(nn.ReflectionPad2d(1),
            spectral_norm(nn.Conv2d(in_channels=dim, out_channels=dim, kernel_size=3, padding=0, dilation=1, bias=not use_spectral_norm), use_spectral_norm),
            nn.InstanceNorm2d(dim, track_running_stats=False)))

        self.conv_block = nn.Sequential(*res)

    def forward(self, x):
        out = x + self.conv_block(x)

        # Remove ReLU at the end of the residual block
        # http://torch.ch/blog/2016/02/04/resnets.html

        return out


def spectral_norm(module, mode=True):
    if mode:
        return nn.utils.spectral_norm(module)

    return module



class UNet(nn.Module):
    def __init__(self, in_channels, n_classes, bilinear=True, sigmoid=False, act_layer=None):
        super(UNet, self).__init__()
        self.n_channels = in_channels
        self.n_classes = n_classes
        self.bilinear = bilinear
        self.sigmoid = sigmoid

        self.inc = DoubleConv(in_channels, 32, act_layer=act_layer)
        self.down1 = Down(32, 64, act_layer=act_layer)
        self.down2 = Down(64, 128, act_layer=act_layer)
        self.down3 = Down(128, 256, act_layer=act_layer)
        self.down4 = Down(256, 512, act_layer=act_layer)
        factor = 2 if bilinear else 1
        self.down5 = Down(512, 1024// factor, act_layer=act_layer)
#        self.up1 = Up(1024, 512 // factor, bilinear)
#        self.up2 = Up(512, 256 // factor, bilinear)
#        self.up3 = Up(256, 128 // factor, bilinear)
#        self.up4 = Up(128, 64, bilinear)
        self.up1 = Up(1024, 512 // factor, bilinear, act_layer=act_layer)
        self.up2 = Up(512, 256 // factor, bilinear, act_layer=act_layer)
        self.up3 = Up(256, 128 // factor, bilinear, act_layer=act_layer)
        self.up4 = Up(128, 64 // factor, bilinear, act_layer=act_layer)
        self.up5 = Up(64, 32, bilinear, act_layer=act_layer)
        self.outc = OutConv(32, n_classes)

    def forward(self, x):
        x1 = self.inc(x)
        x2 = self.down1(x1)
        x3 = self.down2(x2)
        x4 = self.down3(x3)
        x5 = self.down4(x4)
        x6 = self.down5(x5)
        x = self.up1(x6, x5)
        x = self.up2(x, x4)
        x = self.up3(x, x3)
        x = self.up4(x, x2)
        x = self.up5(x, x1)
        x = self.outc(x)
        if self.sigmoid == 1:
            x = torch.sigmoid(x)
        if self.sigmoid == 2:
            x = x
        else:
            x = torch.tanh(x)
        return x


class DoubleConv(nn.Module):
    """(convolution => [BN] => ReLU) * 2"""

    def __init__(self, in_channels, out_channels, mid_channels=None, act_layer=None):
        super().__init__()
        if not mid_channels:
            mid_channels = out_channels


#        self.double_conv = nn.Sequential(
#            nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1, bias=False),
#            nn.BatchNorm2d(mid_channels),
#            self.activation,
#            nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1, bias=False),
#            nn.BatchNorm2d(out_channels),
#            self.activation
#        )

        dc = []
        dc.append( nn.Sequential(nn.Conv2d(in_channels, mid_channels, kernel_size=3, padding=1, bias=False),
            nn.BatchNorm2d(mid_channels)))

        if act_layer == 'elu':
            dc.append(nn.ELU(True))
        elif act_layer == 'relu':
            dc.append(nn.ReLU(True))
        else:
            dc.append(nn.LeakyReLU(inplace=True))

        dc.append(nn.Sequential(nn.Conv2d(mid_channels, out_channels, kernel_size=3, padding=1, bias=False),
            nn.BatchNorm2d(out_channels)))

        if act_layer == 'elu':
            dc.append(nn.ELU(True))
        elif act_layer == 'relu':
            dc.append(nn.ReLU(True))
        else:
            dc.append(nn.LeakyReLU(inplace=True))

        self.double_conv = nn.Sequential(*dc)

    def forward(self, x):
        return self.double_conv(x)


class Down(nn.Module):
    """Downscaling with maxpool then double conv"""

    def __init__(self, in_channels, out_channels, act_layer=None):
        super().__init__()
        self.maxpool_conv = nn.Sequential(
#            nn.MaxPool2d(2),
            BlurPool(in_channels, stride=2, filter_size = 2),
            DoubleConv(in_channels, out_channels, act_layer=act_layer)
        )

    def forward(self, x):
        return self.maxpool_conv(x)


class Up(nn.Module):
    """Upscaling then double conv"""

    def __init__(self, in_channels, out_channels, bilinear=None, act_layer=None):
        super().__init__()
#        if bilinear == True:
#            bilinear = 'bilinear'
        # if bilinear, use the normal convolutions to reduce the number of channels
        if bilinear == 'bilinear':
            self.up = nn.Upsample(scale_factor=2, mode='bilinear', align_corners=True)
            self.conv = DoubleConv(in_channels, out_channels, in_channels // 2, act_layer=act_layer)
        elif bilinear == 'nearest':
            self.up = nn.Upsample(scale_factor=2, mode='nearest')
            self.conv = DoubleConv(in_channels, out_channels, in_channels // 2, act_layer=act_layer)
        else:
            self.up = nn.ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2)
            self.conv = DoubleConv(in_channels, out_channels, act_layer=act_layer)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        # input is CHW
        diffY = x2.size()[2] - x1.size()[2]
        diffX = x2.size()[3] - x1.size()[3]

        x1 = F.pad(x1, [diffX // 2, diffX - diffX // 2,
                        diffY // 2, diffY - diffY // 2])
        # if you have padding issues, see
        # https://github.com/HaiyongJiang/U-Net-Pytorch-Unstructured-Buggy/commit/0e854509c2cea854e247a9c615f175f76fbb2e3a
        # https://github.com/xiaopeng-liao/Pytorch-UNet/commit/8ebac70e633bac59fc22bb5195e513d5832fb3bd
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class OutConv(nn.Module):
    def __init__(self, in_channels, out_channels):
        super(OutConv, self).__init__()
        self.conv = nn.Conv2d(in_channels, out_channels, kernel_size=1)

    def forward(self, x):
        return self.conv(x)

def get_antialiasing_filter(kernel_size):
    """Get an integer specifying the 2D kernel size >>> returns a (1 x 1 x kernel_size x kernel_size)"""

    kernel_dict = {
        1: [[[[1.]]]],

        2: [[[[0.2500, 0.2500],
              [0.2500, 0.2500]]]],

        3: [[[[0.0625, 0.1250, 0.0625],
              [0.1250, 0.2500, 0.1250],
              [0.0625, 0.1250, 0.0625]]]],

        4: [[[[0.0156, 0.0469, 0.0469, 0.0156],
              [0.0469, 0.1406, 0.1406, 0.0469],
              [0.0469, 0.1406, 0.1406, 0.0469],
              [0.0156, 0.0469, 0.0469, 0.0156]]]],

        5: [[[[0.0039, 0.0156, 0.0234, 0.0156, 0.0039],
              [0.0156, 0.0625, 0.0938, 0.0625, 0.0156],
              [0.0234, 0.0938, 0.1406, 0.0938, 0.0234],
              [0.0156, 0.0625, 0.0938, 0.0625, 0.0156],
              [0.0039, 0.0156, 0.0234, 0.0156, 0.0039]]]],

        6: [[[[0.0010, 0.0049, 0.0098, 0.0098, 0.0049, 0.0010],
              [0.0049, 0.0244, 0.0488, 0.0488, 0.0244, 0.0049],
              [0.0098, 0.0488, 0.0977, 0.0977, 0.0488, 0.0098],
              [0.0098, 0.0488, 0.0977, 0.0977, 0.0488, 0.0098],
              [0.0049, 0.0244, 0.0488, 0.0488, 0.0244, 0.0049],
              [0.0010, 0.0049, 0.0098, 0.0098, 0.0049, 0.0010]]]],

        7: [[[[0.0002, 0.0015, 0.0037, 0.0049, 0.0037, 0.0015, 0.0002],
              [0.0015, 0.0088, 0.0220, 0.0293, 0.0220, 0.0088, 0.0015],
              [0.0037, 0.0220, 0.0549, 0.0732, 0.0549, 0.0220, 0.0037],
              [0.0049, 0.0293, 0.0732, 0.0977, 0.0732, 0.0293, 0.0049],
              [0.0037, 0.0220, 0.0549, 0.0732, 0.0549, 0.0220, 0.0037],
              [0.0015, 0.0088, 0.0220, 0.0293, 0.0220, 0.0088, 0.0015],
              [0.0002, 0.0015, 0.0037, 0.0049, 0.0037, 0.0015, 0.0002]]]]

    }

    if kernel_size in kernel_dict:
        return torch.Tensor(kernel_dict[kernel_size])
    else:
        raise ValueError('Unrecognized kernel size')


class BlurPool(nn.Module):

    def __init__(self, channels, stride, filter_size=5):
        super(BlurPool, self).__init__()
        self.channels = channels  # same input and output channels
        self.filter_size = filter_size
        self.stride = stride
        '''Kernel is a 1x5x5 kernel'''

        # repeat tensor from (1 x 1 x fs x fs) >>> (channels x 1 x fs x fs)
        self.kernel = nn.Parameter(get_antialiasing_filter(filter_size).repeat(self.channels, 1, 1, 1),
                                   requires_grad=False)

    def forward(self, x):
        """
        x is a tensor of dimension (batch, in_channels, height, width)
        - assume same input and output channels, and groups = 1
        - CURRENTLY DON'T SUPPORT PADDING
        """

        y = F.conv2d(input=x, weight=self.kernel, stride=self.stride, groups=self.channels)
        return y

    def to(self, dtype):
        self.kernel = self.kernel.to(dtype)
        return self


